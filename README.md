# Cleaning-App Portal Documentation

Table of Contents:

- [Technology Stack](#markdown-header-technology)
- [Set Up Instructions](#markdown-header-set-up-instructions)
- [Deployment](#markdown-header-deployment)

# Technology

* Ruby 2.3.3
* Rails 5.1.2
* PostgreSQL 9.6.1
* Bootstrap 3

# Set Up Instructions

### Installation

These instructions have been tested on OSX and Linux, other environments may require some modifications. Windows is untested.

### Clone the Repo

`git clone git@github.com:danest/servus.git`
`git checkout master # this is the most up-to-date branch`

### Install Gems
Once you have the project running, install gems:

`bundle install`

### Set Up the Database

Set up the database based on your local configurations:

`cp config/database.yml.example config/database.yml`
`bundle exec rake db:create`
`bundle exec rake db:setup`


### Starting the App

`bundle exec rails s`

It will start app by default at <http://localhost:3000>


# Deployment

Application is running on XXXXXXXX. You will need to have your ssh key on the server to be able to deploy

To deploy to staging use:
-	bundle exec cap staging deploy

To deploy to production use:
-	bundle exec cap production deploy


To connect to the console in staging use:
- bundle exec cap staging rails:console

To connect to the console in production use:
- bundle exec cap production rails:console


# Connecting to the Server

To connect to the server you will need to have your ssh keys installed on the server.

For staging:

ssh deploy@XXX.XXX.XXX.XXX

For production:

ssh deploy@XXX.XXX.XXX.XXX

