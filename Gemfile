source 'https://rubygems.org'

gem 'rails', '~> 5.1.2'
gem 'pg', '~> 0.18'
gem 'puma', "3.10.0"
gem 'bootstrap-sass', '~> 3.3.6'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'jbuilder', '~> 2.5'
gem 'haml'
gem 'simple_form'
gem 'jquery-rails', '~> 4.3', '>= 4.3.1'
gem 'devise'
gem 'exception_notification'
gem 'slack-notifier'
gem 'activeadmin', git: 'https://github.com/activeadmin/activeadmin.git'
gem 'active_record-acts_as'
gem 'jquery-ui-rails'
gem 'ancestry'
gem 'fullcalendar-rails'
gem 'redis', '~>3.2'
gem 'resque', :require => "resque/server"
gem 'listen', '>= 3.0.5', '< 3.2'
gem 'faker'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.47'
gem 'dotenv-rails'
gem 'twilio-ruby', '~> 5.4.0'
gem 'kaminari'
gem 'whenever', :require => false
gem 'simplecov', :require => false, :group => :test
gem 'honeybadger', '~> 3.1'
gem 'paper_trail'
gem 'faraday', '~> 0.14.0'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rspec-rails', '~> 3.5'
  gem 'factory_bot_rails'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem "better_errors"
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-rails-console', require: false
  gem "capistrano-resque", "~> 0.2.2", require: false
end

group :test do
  gem 'database_cleaner'
  gem 'shoulda-matchers', git: 'https://github.com/thoughtbot/shoulda-matchers.git', branch: 'rails-5'
  gem 'rails-controller-testing'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
