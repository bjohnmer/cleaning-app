# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Manager.create(first_name: 'User',last_name:'Test', email:'test@email.com',password:'test1234', password_confirmation:'test1234', role:'manager')

Config.create(key: 'cancelation_penalty_time', value: '12')
Config.create(key: 'cancelation_penalty_amount', value: '120')
Config.create(key: 'show_adress_time', value: '20')
Config.create(key: 'requested_booking_limit', value: '20')
Config.create(key: 'send_emails', value: 'false')