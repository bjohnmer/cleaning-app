class AddArchivedToService < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :archived, :boolean, default: false
  end
end
