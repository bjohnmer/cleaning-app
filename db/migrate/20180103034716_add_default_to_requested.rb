class AddDefaultToRequested < ActiveRecord::Migration[5.1]
  def up
	  change_column :bookings, :requested, :boolean, default: false
	  change_column :bookings, :booking_assigned, :boolean, default: false
	end

	def down
		change_column :bookings, :requested, :boolean, default: nil
		change_column :bookings, :booking_assigned, :boolean, default: nil
	end
end
