class AddInformationToServices < ActiveRecord::Migration[5.1]
  def change
    add_column :services, :quantity_based, :boolean, default: false
    add_column :services, :description, :text
    add_column :services, :staff_only, :boolean, default: false
    add_column :services, :company_id, :integer 
  end
end
