class CreateZipcodes < ActiveRecord::Migration[5.1]
  def change
    create_table :zipcodes do |t|
      t.integer :number
      t.references :area, foreign_key: true

      t.timestamps
    end
  end
end
