class AddMinutesToPermission < ActiveRecord::Migration[5.1]
  def change
    add_column :permissions, :minutes, :string
  end
end
