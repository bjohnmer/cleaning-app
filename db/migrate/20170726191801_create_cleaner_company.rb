class CreateCleanerCompany < ActiveRecord::Migration[5.1]
  def change
    create_table :cleaner_companies do |t|
      t.belongs_to :cleaner, foreign_key: true
      t.belongs_to :company, foreign_key: true
      t.timestamps
    end
  end
end
