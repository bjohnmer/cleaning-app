class CreateTableBookingsTags < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings_tags do |t|
      t.belongs_to :booking, index: true
      t.belongs_to :tag, index: true
    end
  end
end
