class SetCleanersReceiveEmailToFalse < ActiveRecord::Migration[5.1]
  def up
    Cleaner.update_all(send_emails: false)
  end
 
  def down
    Cleaner.update_all(send_emails: true)
  end
end
