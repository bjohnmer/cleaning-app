class CreateTableBookingServices < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings_services do |t|
      t.belongs_to :booking, index: true
      t.belongs_to :service, index: true
    end
  end
end
