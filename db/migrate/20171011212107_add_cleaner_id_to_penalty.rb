class AddCleanerIdToPenalty < ActiveRecord::Migration[5.1]
  def change
  	add_column :penalties, :cleaner_id, :integer
  end
end
