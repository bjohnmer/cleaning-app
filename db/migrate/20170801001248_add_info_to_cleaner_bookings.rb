class AddInfoToCleanerBookings < ActiveRecord::Migration[5.1]
  def change
  	add_column :cleaner_bookings, :earnings, :decimal, precision: 15, scale: 2
  end
end
