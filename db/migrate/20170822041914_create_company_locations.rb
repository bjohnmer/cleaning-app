class CreateCompanyLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :companies_locations do |t|
      t.belongs_to :company, index: true
      t.belongs_to :location, index: true
    end
  end
end
