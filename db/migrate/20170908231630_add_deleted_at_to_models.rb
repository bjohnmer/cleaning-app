class AddDeletedAtToModels < ActiveRecord::Migration[5.1]
  def change
    add_column :cleaners, :archived, :boolean, default: false
    add_column :customers, :archived, :boolean, default: false
    add_column :bookings, :archived, :boolean, default: false
  end
end