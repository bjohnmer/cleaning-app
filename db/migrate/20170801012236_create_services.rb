class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :name
      t.decimal :price, precision: 15, scale: 2
      t.integer :service_type

      t.timestamps
    end
  end
end
