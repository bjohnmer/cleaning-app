class CreateBookingServices < ActiveRecord::Migration[5.1]
  def change
    create_table :booking_services do |t|
      t.belongs_to :booking, index: true
      t.belongs_to :service, index: true
      t.integer :hours, default: 1

      t.timestamps
    end
  end
end
