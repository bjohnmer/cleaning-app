class AddRequestedToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :requested, :boolean
  end
end
