class ChangeLocationOnCompany < ActiveRecord::Migration[5.1]
  def change
    remove_column :companies, :location, :string
    add_column :companies, :location, :integer
  end
end
