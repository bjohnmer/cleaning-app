class AddServiceIdToServices < ActiveRecord::Migration[5.1]
  def change
    add_reference :services, :service, index: true
  end
end
