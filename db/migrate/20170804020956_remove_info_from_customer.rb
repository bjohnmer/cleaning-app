class RemoveInfoFromCustomer < ActiveRecord::Migration[5.1]
  def change
    remove_column :customers, :address
    remove_column :customers, :postal_code
    remove_column :customers, :city
    remove_column :customers, :state
  end
end
