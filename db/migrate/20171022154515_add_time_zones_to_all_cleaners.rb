class AddTimeZonesToAllCleaners < ActiveRecord::Migration[5.1]
  def change
    reversible do |dir|
      dir.up do
        Cleaner.all.each do |cleaner|
          cleaner.timezone = 'Eastern Time (US & Canada)'
          cleaner.save
        end
      end
      dir.down do
        Cleaner.all.each do |cleaner|
          cleaner.timezone = nil
          cleaner.save
        end
      end
    end
  end
end