class AddSendEmailsToCleaner < ActiveRecord::Migration[5.1]
  def change
    add_column :cleaners, :send_emails, :boolean, default: true
  end
end
