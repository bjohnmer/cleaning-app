class CreateBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :bookings do |t|
      t.string :location
      t.date :booking_date
      t.time :booking_time
      t.float :rating_value
      t.text :rating_comment
      t.decimal :service_total, precision: 15, scale: 2
      t.decimal :extras_total, precision: 15, scale: 2
      t.decimal :sub_total, precision: 15, scale: 2
      t.decimal :sales_tax, precision: 15, scale: 2
      t.decimal :final_amount, precision: 15, scale: 2
      t.decimal :amount_paid_by_customer, precision: 15, scale: 2
      t.decimal :amount_paid_by_customer, precision: 15, scale: 2
      t.decimal :amount_owed_by_customer, precision: 15, scale: 2
      t.decimal :tip, precision: 15, scale: 2
      t.string :payment_method
      t.string :frecuency
      t.decimal :discount_from_code, precision: 15, scale: 2
      t.decimal :discount_from_frecuency, precision: 15, scale: 2
      t.decimal :discount_from_referral, precision: 15, scale: 2
      t.decimal :giftcard_amount_used, precision: 15, scale: 2
      t.date :next_booking
      t.text :staff_notes
      t.text :customer_notes
      t.string :flexibility
      t.string :entrance_method
      t.boolean :booking_assigned
      t.belongs_to :property, index: true

      t.timestamps
    end
  end
end
