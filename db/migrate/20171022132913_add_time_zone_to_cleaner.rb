class AddTimeZoneToCleaner < ActiveRecord::Migration[5.1]
  def change
    add_column :cleaners, :timezone, :integer, default: "Eastern Time (US & Canada)"
  end
end
