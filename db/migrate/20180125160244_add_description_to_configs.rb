class AddDescriptionToConfigs < ActiveRecord::Migration[5.1]
  def change
    add_column :configs, :description, :text
  end
end
