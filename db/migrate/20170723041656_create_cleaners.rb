class CreateCleaners < ActiveRecord::Migration[5.1]
  def change
    create_table :cleaners do |t|
  		t.string :phone_number
  	  t.string :address
  	  t.string :postal_code
  	  t.string :state
  	  t.string :city
  	  t.integer :max_bookings, default: 2
  		t.float :rating, default: 0
      t.timestamps
    end
  end
end
