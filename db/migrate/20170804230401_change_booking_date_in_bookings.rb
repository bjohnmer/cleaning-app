class ChangeBookingDateInBookings < ActiveRecord::Migration[5.1]
  def up
    remove_column :bookings, :booking_time
    change_column :bookings, :booking_date, :datetime
    change_column :bookings, :next_booking, :datetime
  end

  def down
    add_column :bookings, :booking_time, :time
    change_column :bookings, :booking_date, :date
    change_column :bookings, :next_booking, :date
  end
end
