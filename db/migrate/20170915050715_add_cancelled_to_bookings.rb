class AddCancelledToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :cancelled, :boolean, default: false
    add_column :bookings, :cancelled_reason, :text
  end
end
