class AddCustomerIdToProperty < ActiveRecord::Migration[5.1]
  def change
  	add_column :properties, :customer_id, :integer
  end
end
