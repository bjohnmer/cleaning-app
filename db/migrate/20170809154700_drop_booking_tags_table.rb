class DropBookingTagsTable < ActiveRecord::Migration[5.1]
  def change
  	drop_table :bookings_tags
  end
end
