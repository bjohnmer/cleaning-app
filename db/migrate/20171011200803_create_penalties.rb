class CreatePenalties < ActiveRecord::Migration[5.1]
  def change
    create_table :penalties do |t|
      t.integer :amount
      t.boolean :paid, default: :false

      t.timestamps
    end
  end
end
