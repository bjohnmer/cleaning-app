class AddLaunch27IdToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :launch_27_id, :integer
  end
end
