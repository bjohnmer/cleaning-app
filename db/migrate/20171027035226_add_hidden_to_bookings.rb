class AddHiddenToBookings < ActiveRecord::Migration[5.1]
  def change
    add_column :bookings, :hidden, :boolean, default: false
  end
end
