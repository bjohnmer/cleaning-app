class AddPercetageOfBookingToCleaners < ActiveRecord::Migration[5.1]
  def change
  	add_column :cleaners, :percentage_of_booking, :integer
  end
end
