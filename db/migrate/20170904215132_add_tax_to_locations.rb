class AddTaxToLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :locations, :tax, :decimal
  end
end
