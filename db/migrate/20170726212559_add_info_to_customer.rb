class AddInfoToCustomer < ActiveRecord::Migration[5.1]
  def change
    add_column :customers, :address, :string
    add_column :customers, :postal_code, :string
    add_column :customers, :city, :string
    add_column :customers, :state, :string
    add_column :customers, :phone, :string
    add_column :customers, :stripe_id, :string
    add_column :customers, :date_created, :date
    add_column :customers, :date_of_first_booking, :date
    add_column :customers, :date_of_last_booking, :date
    add_column :customers, :bookings_frecuency, :string
    add_column :customers, :date_of_next_booking, :date
    add_column :customers, :number_of_bookings, :integer
    add_column :customers, :referral_code, :string
    add_column :customers, :referee_code, :string
    add_column :customers, :notes, :text
  end
end
