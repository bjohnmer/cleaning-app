class CreateCleanerBookings < ActiveRecord::Migration[5.1]
  def change
    create_table :cleaner_bookings do |t|
      t.belongs_to :booking, index: true
      t.belongs_to :cleaner, index: true
      
      t.timestamps
    end
  end
end
