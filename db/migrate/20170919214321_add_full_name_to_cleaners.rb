class AddFullNameToCleaners < ActiveRecord::Migration[5.1]
  def change
    add_column :cleaners, :full_name, :string
  end
end
