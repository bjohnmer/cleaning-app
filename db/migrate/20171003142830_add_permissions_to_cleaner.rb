class AddPermissionsToCleaner < ActiveRecord::Migration[5.1]
  def change
    add_column :cleaners, :permission_id, :integer
  end
end
