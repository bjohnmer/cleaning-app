class Booking < ApplicationRecord
  has_paper_trail
  include Twiliable
  belongs_to :property
  belongs_to :company
  belongs_to :location
  has_many :cleaner_bookings
  has_many :cleaners, through: :cleaner_bookings
  has_many :booking_services
  has_many :services, through: :booking_services
  validates :booking_date, presence: true
  # after_save :calculate_fees
  scope :active, -> { where(archived: false) }
  scope :archived, -> { where(archived: true) }
  after_save :notify_cancellation
  after_save :notify_requested_bookings
  #Penalties
  has_many :penalties

  scope :claimed, -> { where(booking_assigned: true) }
  scope :cancelled, -> { where(cancelled: true) }
  scope :not_cancelled, -> { where(cancelled: false) }
  scope :left_to_claime, -> { where(booking_assigned: false) }
  scope :not_hidden, -> { where(hidden: false) }
  
  def calculate_fees
    service_total =  self.services.select { |service| service.service_type != 'add_on' }.pluck(:price).inject(0){|sum,x| sum + x }
    extras_total = self.services.select { |service| service.service_type == 'add_on' }.pluck(:price).inject(0){|sum,x| sum + x }
    sub_total = service_total + extras_total
    sales_tax = sub_total * ( self.location.tax.to_f / 100 )
    final_amount = sub_total + sales_tax
    self.update_column(:service_total, service_total)
    self.update_column(:extras_total, extras_total)
    self.update_column(:sub_total, sub_total)
    self.update_column(:sales_tax, sales_tax)
    self.update_column(:final_amount, final_amount)
    self.cleaner_bookings.each do |cb|
      earnings = final_amount.to_f * (cb.cleaner.percentage_of_booking.to_f/100)
      cb.update_column(:earnings,earnings)
    end
  end

  def cancel_booking
    if !self.can_cancel_booking?      
      amount = Config.fetch('cancelation_penalty_amount') ? Config.fetch('cancelation_penalty_amount') : 0
      self.cleaners.map {|cleaner| Penalty.create!(amount: amount, cleaner: cleaner, booking: self) }
      Manager.all.map {|manager| BookingMailer.notify_cancelation_to_managers(manager, self).deliver  }
    end
    self.booking_assigned = false
    self.cleaners.destroy_all
  end

  def can_cancel_booking?
    return Config.fetch('cancelation_penalty_time') ? ((self.booking_date - Time.current) / 3600) >= Config.fetch('cancelation_penalty_time').to_i : true
  end

	def notify_cancellation
		if self.cancelled == true && !self.cleaners.empty?
			self.cleaners.map { |cleaner| BookingMailer.notify_cancelation(cleaner, self).deliver  }
			notify_cancelation_via_text(self.cleaners, self)
			self.booking_assigned = false
      self.cleaners.destroy_all
		end
	end

  def check_expired_requested_bookings
    bookings = Booking.where(requested: true)
    bookings.each do |booking|
      if Time.current > (booking.booking_date - Config.fetch('requested_booking_limit').to_i.hours - 1.days)
        booking.update_column(:booking_assigned, false)
        booking.update_column(:requested, false)
        Manager.all.map {|manager| BookingMailer.notify_booking_not_accepted_to_managers(manager, booking).deliver}
        booking.cleaners.destroy_all
      end
    end
    Rails.logger.info("Check Requested updated at #{Time.now}")
  end

  def notify_requested_bookings
    if self.requested == true && !self.cleaners.empty?
      self.cleaners.map { |cleaner| BookingMailer.email_customer_request(cleaner, self).deliver }
      notify_booking_request(self.cleaners, self)
    end
  end

  def accept_request(cleaner)
    self.booking_assigned = true
    self.requested = false
    Manager.all.map {|manager| BookingMailer.notify_claimed_booking_to_managers(manager, self, cleaner).deliver}
  end

  def decline_request(declined_cleaner)
    Manager.all.map {|manager| BookingMailer.notify_declined_booking_to_managers(manager, self, declined_cleaner).deliver}
    self.booking_assigned = false
    self.requested = false
    self.cleaners.destroy_all
  end

  def is_past_booking?
    return self.booking_date < Time.current
  end
end
