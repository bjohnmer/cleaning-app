class Customer < ApplicationRecord
  include Importable
  include Filters
  acts_as :user
  has_many :properties, dependent: :destroy
  accepts_nested_attributes_for :properties
  scope :active, -> { where(archived: false) }
  scope :archived, -> { where(archived: true) }

  def full_name
    return "#{first_name} #{last_name}".titleize
  end
end
