class Company < ApplicationRecord
  has_many :services
  has_many :cleaner_companies
  has_many :bookings
  has_and_belongs_to_many :locations
  has_many :cleaners, through: :cleaner_companies
  accepts_nested_attributes_for :cleaners
  validates :name, presence: true
  validates :name, uniqueness: true
end
