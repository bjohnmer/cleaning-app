module Importable
  extend ActiveSupport::Concern
	require 'csv'

  module ClassMethods
    def import_customer(file)
      customers_created = 0
      row_number = 1
      errors = []
      ActiveRecord::Base.transaction do
        CSV.foreach(file, headers: true) do |row|
          row_number += 1
          row = row.to_hash
          # Customer Params
          first_name = row.values.first
          last_name = row["Last Name"]
          email = row['Email']
  				stripe = row['Stripe ID']
          date_first_booking = row['"Date of First Booking']
          date_last_booking = row['Date of Last Booking']
          frecuency = row['Frequency of Last Booking']
          date_next_booking = row['Date of Next Booking']
          number_of_bookings = row['Number of Bookings']
          referal_code = row['Referral Code']
          referee_code = row['Referee Code']
          created_at = Date.strptime(row['Date Created'], '%m/%d/%Y')
          notes = row['Notes']     
          phone = row['Phone']
  				
          # Property Params
          address = row['Address']
          city = row['City']
          state = row['State']
          postal_code =  row['Postal Code']

          # Tags Information
          tags = row['Tags']

          Rails.logger.info('Creating customer')
          customer = Customer.create_with(
            first_name: first_name,
            last_name: last_name,
            phone: phone,
            role: 'customer',
            stripe_id: stripe,
            date_of_first_booking: date_first_booking,
            date_of_last_booking: date_last_booking,
            bookings_frecuency: frecuency,
            date_of_next_booking: date_next_booking,
            number_of_bookings: number_of_bookings,
            referral_code: referal_code,
            referee_code: referee_code,
            notes: notes,
            created_at: created_at,
          ).find_or_create_by(email: email)

          customer.skip_password_validation = true

          property = Property.new(
            state: state,
            postal_code: postal_code,
            address: address, 
            city: city
          )

          unless customer.properties.pluck(:address).include?(property.address)
            customer.properties << property
          end

          if customer.save
            customers_created += 1
            if tags
              tags.split(', ').each do |tag|
                tag = Tag.find_or_create_by(name: tag)
                property_tag = property.tags.find_by(id: tag.id)

                unless property_tag
                  property.tags << tag
                end
              end
            end
          else
            error = "Error on row number: #{row_number} - #{ email } #{ first_name } #{ last_name }, missing data: #{ customer.errors.full_messages.to_sentence  }"
            errors << error
          end          
        end
      end

      # Send emails to each manager
      Manager.all.each do |m|
        Rails.logger.info("Customer importation email sent: #{m.email}" )
        ImportCustomerMailer.send_email(m, customers_created, errors).deliver
      end

      Rails.logger.info(errors)

      FileUtils.rm(file)
    end

    def import_cleaners(file, companies)
      companies = Company.find(companies.pluck("id"))
      cleaners_created = 0
      row_number = 0
      errors = []
      ActiveRecord::Base.transaction do
        CSV.foreach(file, headers: true) do |row|
          row_number += 1
          full_name = row['Contractor Name'].scan(/([^,]+)/).flatten.map(&:strip)
          last_name = full_name.first
          first_name = full_name.last
          email = row['Email']
          address = row['Street']
          city = row['City']
          state = row['State']
          zip = row['Zip Code']
          status = row['Status']
          archived = status == 'Active' ? false : true  
          cleaner = Cleaner.create_with(
            first_name: first_name,
            last_name: last_name,
            role: 'cleaner',
            address: address, 
            postal_code: zip, 
            state: state, 
            city: city,
            archived: archived
          ).find_or_create_by(email: email)
          cleaner.skip_password_validation = true
          companies.map{ |company| cleaner.companies << company unless cleaner.companies.include?(company) }
          if cleaner.save
            Rails.logger.info("Cleaner #{email} Created")
            cleaners_created += 1
          else
            error = "Error on row number: #{row_number} - #{ email } #{ first_name } #{ last_name }, missing data: #{ cleaner.errors.full_messages.to_sentence  }"
            errors << error
          end
        end
      end

      # Send emails to each manager
      Manager.all.each do |m|
        Rails.logger.info("Cleaner importation email sent: #{m.email}" )
        ImportCleanerMailer.send_email(m, cleaners_created, errors).deliver
      end

      FileUtils.rm(file)
      Rails.logger.info(errors)
    end
  end
end