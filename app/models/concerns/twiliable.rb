module Twiliable
  extend ActiveSupport::Concern
  require 'twilio-ruby'

  def notify_cancelation_via_text(users, booking)
    @client = Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
    users.each do |user|
      if user.phone_number
        begin
          message = "Hi, #{user.full_name}. Unfortunately, your booking for #{booking.property.customer.full_name} has been cancelled. Now you have one new spot available to pick another booking. Hurry up so you dont miss it. "
          @client.api.account.messages.create(
            from: ENV['TWILIO_ACCOUNT_NUMBER'],
            to: user.phone_number,
            body: message,
          )
        rescue Exception
          Rails.logger.info(Exception)
        end
      end
    end
  end

  def notify_booking_request(users, booking)
    @client = Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
    users.each do |user|
      if user.phone_number
        begin
          message = "Hi, #{user.full_name}. You have a requested booking for #{booking.property.customer.full_name}. Please visit www.servuspros.com to accept or decline."
          @client.api.account.messages.create(
            from: ENV['TWILIO_ACCOUNT_NUMBER'],
            to: user.phone_number,
            body: message,
          )
        rescue Exception
          Rails.logger.info(Exception)
        end
      end
    end
  end
end