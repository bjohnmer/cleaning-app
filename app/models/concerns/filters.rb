module Filters
  extend ActiveSupport::Concern

  included do
    SCOPES = [:first_name, :last_name, :email]

    SCOPES.each do |condition_type|
      scope "user_#{condition_type}", lambda { |q|
        joins(:user).merge User.search("name_#{condition_type}" => q).result
      }
    end
  end


  def self.ransackable_scopes(auth_object = nil)
    SCOPES.map{ |k| "user_#{k}" }
  end
end