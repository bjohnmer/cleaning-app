class User < ApplicationRecord
  attr_accessor :skip_password_validation  # virtual attribute to skip password validation while saving
  actable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :first_name, :last_name, :role, presence: true
  enum role: [:customer, :cleaner, :manager]

  def full_name
    return "#{first_name} #{last_name}"
  end

  protected

  def password_required?
    return false if skip_password_validation
    super
  end
end
