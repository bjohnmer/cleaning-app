class Location < ApplicationRecord
  has_and_belongs_to_many :companies
  has_many :bookings
  validates :name, :tax, presence: true
  validates :name, uniqueness: true
  accepts_nested_attributes_for :companies
end
