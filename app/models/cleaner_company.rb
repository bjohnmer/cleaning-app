class CleanerCompany < ApplicationRecord
  belongs_to :cleaner
  belongs_to :company
  validates :cleaner, :company, presence: true
end
