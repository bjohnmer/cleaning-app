class Service < ApplicationRecord
  has_ancestry
  belongs_to :company
  has_many :parameters, :class_name => "Service", :foreign_key => "service_id"
  has_many :booking_services
  has_many :bookings, through: :booking_services
  accepts_nested_attributes_for :parameters
  validates :name, :price, :service_type, presence: true
  enum service_type: [:base, :add_on, :parameter]
  scope :active, -> { where(archived: false) }
  scope :archived, -> { where(archived: true) }
end
