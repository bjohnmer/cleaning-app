class Zipcode < ApplicationRecord
  belongs_to :area
  validates :number, uniqueness: true
end
