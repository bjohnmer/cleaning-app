class Cleaner < ApplicationRecord
  include Importable
  include Filters
  acts_as :user
  # Companies
  has_many :cleaner_companies
  has_many :companies, through: :cleaner_companies, dependent: :destroy
  # Bookings
  has_many :cleaner_bookings
  has_many :bookings, through: :cleaner_bookings
  belongs_to :permission, optional: true
  #Penalties
  has_many :penalties
  accepts_nested_attributes_for :companies
  validates :address, :postal_code, :state, :city, :role, presence: true
  validates :max_bookings, numericality: { greater_than: 0, less_than_or_equal_to: 3 }
  scope :active, -> { where(archived: false) }
  scope :archived, -> { where(archived: true) }
  after_save :get_full_name

  TimeZones = { 
    "International Date Line West" => 1, 
    "Midway Island" => 2, 
    "Hawaii" => 3, 
    "Alaska" => 4, 
    "Pacific Time (US & Canada)" => 5, 
    "Tijuana" => 6, 
    "Mountain Time (US & Canada)" => 7, 
    "Arizona" => 8, 
    "Central Time (US & Canada)" => 9,
    "Eastern Time (US & Canada)" => 10, 
    "Indiana (East)" => 11, 
    "Atlantic Time (Canada)" => 12, 
    "UTC" => 13,
  }
  enum timezone: TimeZones, _suffix: true

  def get_requested_bookings
    # Does not show if time of booking - requested_booking_limit is more than time now.
    # A cron job will deleted the requested booking to put it back in the pool 
    self.bookings.where(["requested = ? AND DATE(booking_date) >= ?", true, Date.today]).select{|b| Time.current < b.booking_date - Config.fetch('requested_booking_limit').to_i.hours }
  end

  #Show Previos bookings Assigned
  def get_previous_bookings
    return self.bookings.where(["DATE(booking_date) <= ? ", 
      Date.today - 1.day ]).order(:booking_date)
  end

  # show the bookings assigned for today.
  def get_todays_bookings
    return self.bookings.where(["DATE(booking_date) <= ? AND DATE(booking_date) >= ?  AND requested = ?", 
      Date.today + 1.day, Date.today - 1.day, false ]).order(:booking_date).select{ |booking| 
      booking.booking_date.localtime.to_date ==  Time.now.to_date }
  end

  def get_assigned_bookings_for_tomorrow
    return self.bookings.where(["DATE(booking_date) >= ? AND requested = ?", Date.today - 1, false ]).order(:booking_date).
    select{ |booking| booking.booking_date.localtime.to_date ==  Time.now.to_date + 1 }
  end

  def get_tomorrow_bookings
    return self.bookings.where(["DATE(booking_date) <= ? AND DATE(booking_date) >= ? AND requested = ?", 
      Time.now.localtime.to_date.end_of_day + 2.days, Time.now.localtime.to_date + 2.day, false])
  end

  def get_bookings_by_date(start_time, end_time)
    return self.bookings.where(["DATE(booking_date) <= ? AND DATE(booking_date) >= ? AND requested = ?", 
      start_time, end_time, false])
  end

  # show tomorrows_available bookings
  def available_bookings(from: nil, to: nil)
    start_time = from ? from : Time.now.localtime.beginning_of_day + 2.days
    end_time = to ? to : Time.now.localtime.end_of_day + 2.days
    ready_tomorrow_booking_time = {}
    ready_tomorrow_booking_time[:max] = self.get_tomorrow_bookings.blank? ? false : self.get_tomorrow_bookings.order(booking_date: :desc).first.booking_date.localtime + 4.hours
    ready_tomorrow_booking_time[:min] = self.get_tomorrow_bookings.blank? ? false : self.get_tomorrow_bookings.order(booking_date: :asc).first.booking_date.localtime - 4.hours
    companies = self.companies.empty? ? '(0)' : self.companies.pluck(:id).to_s.tr('[]', '()')
    bookings = Booking.where(
      ["company_id IN #{companies} AND booking_date >= ? AND booking_date <= ? AND booking_assigned = ? AND cancelled = ? AND archived = ? AND requested = ?", 
       start_time, end_time, false, false, false, false ]
    ).order(:booking_date)
    unless ready_tomorrow_booking_time[:max].blank? || ready_tomorrow_booking_time[:min].blank?
      bookings = bookings.select{|b| b.booking_date.localtime.to_time >= ready_tomorrow_booking_time[:max].to_time || b.booking_date.localtime.to_time <= ready_tomorrow_booking_time[:min].to_time }
    end
    return bookings
  end

  def pick_booking(booking)
    booking.booking_assigned = true
    self.bookings << booking
    booking.calculate_fees
    Manager.all.map {|manager| BookingMailer.notify_claimed_booking_to_managers(manager, booking, self).deliver}
  end

  #Check if cleaner can claim booking base on max number of bookings he/she can take
  def can_claim_booking?
    today_bookings =  self.bookings.where(["DATE(booking_date) <= ? AND DATE(booking_date) >= ? AND requested = ? ", Date.today + 2, Date.today - 1, false ])
    .select{|b| b.booking_date.localtime.to_date == Date.today + 2}.count
    return today_bookings < self.max_bookings
  end

  # Check if they have permissions to see the bookings
  def has_permission_to_view?
    return false if !self.permission
    return Time.current.utc.in_time_zone(self.timezone).strftime( "%H%M%S%N" ) >= (self.permission.hour + ':' + self.permission.minutes).to_time.strftime( "%H%M%S%N" ) if self.permission.hour && self.permission.minutes 
  end

  def cut_of_time
    return (Time.current.utc.in_time_zone(self.timezone) + 1.day ).change({ hour: Config.fetch('cut_of_time').to_i }) >= Time.current.utc.in_time_zone(self.timezone)
  end

  def bookings_left_to_claim
    return max_bookings - get_tomorrow_bookings.count
  end

  def bookings_left_to_claim_by_date(time_start, time_end)
    number_of_bookings = get_bookings_by_date(time_start, time_end).count
    return max_bookings - number_of_bookings
  end


  def email_today_bookings(bookings)
    bookings = Booking.find(bookings)
    BookingMailer.email_today_bookings(self, bookings).deliver
  end

  def email_tomorrow_bookings(bookings)
    bookings = Booking.find(bookings)
    BookingMailer.email_tomorrow_bookings(self, bookings).deliver
  end

  private

  def get_full_name
    full_name = "#{first_name} #{last_name}"
    self.update_column(:full_name, full_name)
  end
end
