class Area < ApplicationRecord
  has_many :zipcodes, dependent: :destroy
  accepts_nested_attributes_for :zipcodes
end
