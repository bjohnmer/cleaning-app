class Property < ApplicationRecord
  belongs_to :customer
  has_many :bookings
  has_and_belongs_to_many :tags
  validates :address, :city, :state, :postal_code, presence: true

  def full_address
    return "#{address}, #{city}, #{state}, #{postal_code}".titleize
  end
end
