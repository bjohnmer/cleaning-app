class Config < ApplicationRecord
  validates :key, :value, presence: true
  validates :key, uniqueness: true

  def self.store(key,value)
    config = Config.find_by_key(key)
    config.value = value
    config.save
  end

  def self.fetch(key)
    config = Config.find_by_key(key)
    return config ? Config.find_by_key(key).value : nil
  end
end
