class Penalty < ApplicationRecord
	belongs_to :cleaner
  belongs_to :booking
	validates :amount, presence: true
end
