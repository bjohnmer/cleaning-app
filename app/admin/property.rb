ActiveAdmin.register Property do
  permit_params :address, :city, :state, :postal_code, :customer_id

  show do
    attributes_table do
      row :address
      row :city
      row :state
      row :postal_code
      row :customer
    end
    panel "Tags" do
      tags = property.tags
      table_for tags do
        column :name
      end
    end
  end

end
