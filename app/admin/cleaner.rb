ActiveAdmin.register Cleaner do
  permit_params :first_name, :last_name, :role, :email, :password, :password_confirmation, :send_emails,
    :phone_number, :timezone, :address, :postal_code, :state, :city, :max_bookings, :rating, :archived, :permission_id, :percentage_of_booking, company_ids: []

  scope :active, default: true
  scope :archived

  filter :user_first_name_cont, label: 'First Name'
  filter :user_last_name_cont, label: 'Last Name'
  filter :user_email_cont, label: 'Email'
  filter :companies

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :email
    column "Companies" do |cleaner|
      cleaner.companies.map { |c| c.name }.join("<br>").html_safe
    end
    column "" do |resource|
      links = ''.html_safe
      links += link_to 'View', resource_path(resource), class: "member_link show_link"
      links += link_to 'Edit', edit_resource_path(resource), class: "member_link edit_link"
      if resource.archived?
        links += link_to 'Unarchived', unarchive_cleaner_admin_cleaners_path(resource), confirm: 'Are you sure that you want to unarchive The Cleaner', class: "member_link archived_link"
        links += link_to 'Delete', resource_path(resource), method: :delete, confirm: I18n.t('active_admin.delete_confirmation'), class: "member_link delete_link"
      else
        links += link_to 'Archive', archive_cleaner_admin_cleaners_path(resource), confirm: 'Are you sure that you want to Archive The Cleaner', class: "member_link archived_link"
      end
      links += link_to 'Login as', login_as_admin_cleaner_path(resource), class: "member_link edit_link"
      links
    end
  end

  collection_action :unarchive_cleaner, method: :get do
    cleaner = Cleaner.find(params[:format])
    cleaner.update_column(:archived, false)
    cleaner.save!
    redirect_to admin_cleaners_path, notice: 'Cleaner was successfully activated'
  end

  collection_action :archive_cleaner, method: :get do
    cleaner = Cleaner.find(params[:format])
    cleaner.update_column(:archived, true)
    cleaner.save!
    redirect_to admin_cleaners_path, notice: 'Cleaner was successfully archived'
  end

  #Method
  member_action :login_as do
    cleaner_user = Cleaner.find(params["id"])
    session["admin_session_id"]=current_user.id
    session["cleaner_session_id"]=cleaner_user.id
    sign_in(:user, cleaner_user)
    redirect_to root_path
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :full_name
      row :email
      row :phone_number
      row :address
      row :postal_code
      row :state
      row :city
      row :max_bookings
      row :rating
      row :percentage_of_booking
      row :permission
      row :timezone
    end
    panel "Companies" do
      companies = cleaner.companies
      table_for companies do
        column :name
      end
    end
    panel "Penalties" do
      penalties = cleaner.penalties
      table_for penalties do
        column :amount
        column :paid
        column :booking
      end
    end
    panel "Future Bookings" do
      bookings = cleaner.bookings.select {|b| b.booking_date > Date.today }
      table_for bookings do
        column :location
        column :property
        column :booking_date
        column :final_amount
        column "Earnings" do |booking|
          booking.cleaner_bookings.first.earnings
        end
      end
    end
    panel "Past Bookings" do
      bookings = cleaner.bookings.select {|b| b.booking_date < Date.today }

      table_for bookings do
        column :location
        column :booking_date
        column :property
        column :final_amount
        column "Earnings" do |booking|
          booking.cleaner_bookings.first.earnings
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Cleaner Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :phone_number
      f.input :address
      f.input :postal_code
      f.input :state
      f.input :city
      f.input :archived
      f.input :max_bookings, min: 0, max: 3, step: 1
      f.input :rating, min: 0, max: 5, step: 1
      f.input :role, :input_html => { :value => :cleaner }, as: :hidden
      f.input :password
      f.input :password_confirmation
      f.input :companies, :label => 'Companies', :as => :check_boxes, :collection => Company.all.map{|c| ["#{c.name}", c.id]}
      f.input :permission
      f.input :percentage_of_booking
      f.input :timezone
      f.input :send_emails
    end
    f.actions
  end

  ###### Custom Views ######

  # Import Process
  action_item 'import_cleaner', only: :index do
    link_to 'Import Cleaner', import_admin_cleaners_path
  end

  collection_action :import, method: :get do
    # Just render - no specific implementation
  end

  collection_action :import_process, method: :post do
    file =  params[:import_cleaner][:file]
    companies = Company.find(params[:import_cleaner][:companies].reject(&:empty?))
    if file
      #Create Directory to store files
      importer_dir_path = Rails.root.join('tmp', 'importers')
      FileUtils.mkdir_p importer_dir_path unless File.directory? importer_dir_path
      #get file name
      new_file = CopyTempFile.copy_file(file, importer_dir_path)
      
      #send new file to import job
      if File.exists?(new_file)
        Resque.enqueue(ImportCleaner, new_file, companies)
        flash[:notice] = "Import Process will be completed shortly. An email will be send once it is finished."
      else
        flash[:error] = "#{new_file} does not exists"
      end
      redirect_to admin_cleaners_path
    else
      flash[:error] = 'No file was uploaded'
      redirect_to import_admin_customers_path
    end
  end

  controller do   
    def update
      if params[:cleaner][:password].blank? && params[:cleaner][:password_confirmation].blank?
        params[:cleaner].delete(:password)
        params[:cleaner].delete(:password_confirmation)
      end
      super
    end
  end
end
