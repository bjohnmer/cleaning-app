ActiveAdmin.register Manager do
  permit_params :first_name, :last_name, :role, :email, :password, :password_confirmation

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :email
    column :role
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :user_first_name_cont, label: 'First Name'
  filter :user_last_name_cont, label: 'Last Name'
  filter :user_email_cont, label: 'Email'

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :role, :input_html => { :value => :manager }, as: :hidden
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

  controller do
    def update
      if params[:manager][:password].blank? && params[:manager][:password_confirmation].blank?
        params[:manager].delete(:password)
        params[:manager].delete(:password_confirmation)
      end
      super
    end
  end
end
