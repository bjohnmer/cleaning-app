ActiveAdmin.register Customer do
  permit_params :first_name, :last_name, :role, :email, :password, :password_confirmation,
    :phone, :stripe_id, :date_created, :date_of_first_booking, :date_of_last_booking, 
    :bookings_frecuency, :date_of_next_booking, :number_of_bookings, :referral_code, :referee_code,
    :notes, properties_attributes: [ :address, :city, :state, :postal_code ]
  scope :active, default: true
  scope :archived
  
  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :email
    column "" do |resource|
      links = ''.html_safe
      links += link_to 'View', resource_path(resource), class: "member_link show_link"
      links += link_to 'Edit', edit_resource_path(resource), class: "member_link edit_link"
      if resource.archived?
        links += link_to 'Unarchived', unarchive_customer_admin_customers_path(resource), confirm: 'Are you sure that you want to unarchive the Customer', class: "member_link archived_link"
        links += link_to 'Delete', resource_path(resource), method: :delete, confirm: I18n.t('active_admin.delete_confirmation'), class: "member_link delete_link"
      else
        links += link_to 'Archive',  archive_customer_admin_customers_path(resource), confirm: 'Are you sure that you want to Archive the Customer', class: "member_link archived_link"
      end
      links
    end
  end

  collection_action :unarchive_customer, method: :get do
    cleaner = Customer.find(params[:format])
    cleaner.update_column(:archived, false)
    cleaner.save!
    redirect_to admin_customers_path, notice: 'Customer was successfully activated'
  end

  collection_action :archive_customer, method: :get do
    cleaner = Customer.find(params[:format])
    cleaner.update_column(:archived, true)
    cleaner.save!
    redirect_to admin_customers_path, notice: 'Customer was successfully archived'
  end

  filter :user_first_name_cont, label: 'First Name'
  filter :user_last_name_cont, label: 'Last Name'
  filter :user_email_cont, label: 'Email'

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :email
      row :phone
      row :stripe_id
      row :date_created
      row :date_of_first_booking
      row :date_of_last_booking
      row :bookings_frecuency
      row :date_of_next_booking
      row :number_of_bookings
      row :referral_code
      row :referee_code
      row :notes
    end
    panel "Properties" do
      properties = customer.properties
      table_for properties do
        column :address
        column :city
        column :state
        column :postal_code
      end
    end
    panel "Bookings" do
      bookings = Booking.where(property_id: customer.properties.pluck(:id))
      table_for bookings do
        column 'id' do |booking|
          link_to(booking.id, admin_booking_path(booking.id))
        end
        column :property
        column :location
        column :booking_date
        column :booking_time
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Customer Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :password_confirmation
      f.input :role, :input_html => { :value => :customer }, as: :hidden
      f.input :phone
      f.input :stripe_id
      f.input :date_created
      f.input :date_of_first_booking
      f.input :date_of_last_booking
      f.input :bookings_frecuency
      f.input :date_of_next_booking
      f.input :number_of_bookings
      f.input :referral_code
      f.input :referee_code
      f.input :notes
      f.has_many :properties do |property|
        property.input :address
        property.input :city
        property.input :state
        property.input :postal_code
      end
    end
    f.actions
  end

  ###### Custom Views ######

  # Import Process
  action_item 'import_customers', only: :index do
    link_to 'Import Customers', import_admin_customers_path
  end

  collection_action :import, method: :get do
    # Just render - no specific implementation
  end

  collection_action :import_process, method: :post do
    file =  params[:import_customer][:file]
    if file

      #Create Directory to store files
      importer_dir_path = Rails.root.join('tmp', 'importers')
      FileUtils.mkdir_p importer_dir_path unless File.directory? importer_dir_path
      #get file pathg
      new_file = CopyTempFile.copy_file(file, importer_dir_path)

      #send new file to import job
      if File.exists?(new_file)
        Resque.enqueue(ImportCustomer, new_file)
        flash[:notice] = "Import Process will be completed shortly. An email will be send once it is finished."
      else
        flash[:error] = "#{new_file} does not exists"
      end
      redirect_to admin_customers_path
    else
      flash[:error] = 'No file was uploaded'
      redirect_to import_admin_customers_path
    end
  end


  controller do
    def update
      if params[:customer][:password].blank? && params[:customer][:password_confirmation].blank?
        params[:customer].delete(:password)
        params[:customer].delete(:password_confirmation)
      end
      super
    end
  end
end
