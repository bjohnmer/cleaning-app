ActiveAdmin.register Company do
  permit_params :name, location_ids: []

  index do
    column :id
    column :name
    column "Locations" do |company|
      company.locations.map { |c| "#{c.name}" }.join("<br>").html_safe
    end
    column "Cleaners" do |company|
      company.cleaners.map { |c| "#{c.first_name} #{c.last_name}" }.join("<br>").html_safe
    end
    actions
  end

  show do
    attributes_table do
      row :name
      row "Locations" do |company|
        company.locations.map { |c| "#{c.name}" }.join("<br>").html_safe
      end
    end
    panel "Cleaners" do
      cleaners = company.cleaners
      table_for cleaners do
        column :first_name
        column :last_name
        column :address
        column :phone_number
      end
    end
  end
  
  form do |f|
    f.inputs 'Company' do
      f.input :name
      f.input :locations, :as => :check_boxes, :collection => Location.all.map{|l| ["#{l.name}", l.id]}
    end
    f.actions
  end
end
