ActiveAdmin.register Booking do
  permit_params :location, :booking_date, :rating_value, :rating_comment, :service_total,
    :extras_total, :sub_total, :sales_tax, :final_amount, :amount_paid_by_customer, :amount_paid_by_customer,
    :amount_owed_by_customer, :tip, :payment_method, :frecuency, :discount_from_code, :discount_from_frecuency,
    :discount_from_referral, :giftcard_amount_used, :next_booking, :staff_notes, :customer_notes, :flexibility,
    :entrance_method, :booking_assigned, :property_id, :select_customer, 
    :customer_properties, :customer_firstname, :customer_lastname, :customer_email, :customer_phone, 
    :property_name, :property_address, :property_city, :property_state, :property_postal_code,
    :property_phone,  :select_customer, :customer_properties, :customer_firstname, :customer_lastname, 
    :customer_email,  :customer_phone, :property_name, :property_address, :property_city, 
    :property_state, :property_postal_code, :property_phone, :completed, :cancelled, :requested, :hidden, service_ids: []

  before_action :set_booking, only: [:update]

  scope :active, default: true
  scope :archived
  config.sort_order = 'booking_date_desc'

  index do
    selectable_column
    id_column
    column :location
    column "Booking Date" do |booking|
      booking.booking_date
    end
    column "Customer Name" do |booking|
      booking.property.customer.full_name
    end
    column :booking_assigned
    column :requested
    column :cancelled
    column :final_amount
    column :company
    column "Cleaning Team" do |booking|
      booking.cleaners.map {|c| "#{c.user.first_name} #{c.user.last_name}" }
    end
    column "" do |resource|
      links = ''.html_safe
      links += link_to 'View', resource_path(resource), class: "member_link show_link"
      links += link_to 'Edit', edit_resource_path(resource), class: "member_link edit_link"
      if resource.archived?
        links += link_to 'Unarchived', unarchive_booking_admin_bookings_path(resource), confirm: 'Are you sure that you want to unarchive The Cleaner', class: "member_link archived_link"
        links += link_to 'Delete', resource_path(resource), method: :delete, confirm: I18n.t('active_admin.delete_confirmation'), class: "member_link delete_link"
      else
        links += link_to 'Archive',  archive_booking_admin_bookings_path(resource), confirm: 'Are you sure that you want to Archive The Cleaner', class: "member_link archived_link"
      end
      links
    end
  end

  show do
    attributes_table do
      row :location
      row :booking_date
      row :rating_value
      row :rating_comment
      row :service_total
      row :extras_total
      row :sub_total
      row :sales_tax
      row :final_amount
      row :amount_paid_by_customer
      row :amount_paid_by_customer
      row :amount_owed_by_customer
      row :tip
      row :payment_method
      row :frecuency
      row :discount_from_code
      row :discount_from_frecuency
      row :discount_from_referral
      row :giftcard_amount_used
      row :next_booking
      row :staff_notes
      row :customer_notes
      row :flexibility
      row :entrance_method
      row :booking_assigned
      row :requested
      row :cancelled
      row :completed
      row :cancelled_reason
      row :property
      row :launch_27_id
      row :created_at
      row :hidden
      row 'Launch 27 Link' do |booking|
        company = booking.company.name
        case company
        when "Companion Maids"
          "<a href='https://companionmaids.launch27.com/admin/bookings/#{booking.launch_27_id}/edit' > Go to Launch 27 </a>".html_safe
        when "Cleaning Exec"
          "<a href='https://cleaningexec.launch27.com/admin/bookings/#{booking.launch_27_id}/edit' > Go to Launch 27 </a>".html_safe
        when "Planet Maids"
          "<a href='https://planetmaids.launch27.com/admin/bookings/#{booking.launch_27_id}/edit' > Go to Launch 27 </a>".html_safe
        when 'Maid Sailors'
          "<a href='https://maidsailors.launch27.com/admin/bookings/#{booking.launch_27_id}/edit' > Go to Launch 27 </a>".html_safe
        else
          'No link provided'
        end
      end
    end
    panel "Customer" do
      customer = booking.property.customer
      table_for customer do
        column :name { |c| c.first_name + ' ' + c.last_name }
        column :property { booking.property }
      end
    end
    panel "Services" do
      services = booking.services
      table_for services do
        column :name
        column :price
        column :service_type
      end
    end
    panel "Cleaning Team" do
      cleaners = booking.cleaners
      table_for cleaners do
        column :first_name
        column :last_name
        column :address
        column :phone_number
      end
    end

    panel "Log Changes" do
      versions = booking.versions
      table_for versions do
        column :item_type
        column :event
        column "who did it" do |c| 
          if c.whodunnit == nil
            'Change By Webhook'
          else
            User.find(c.whodunnit).full_name
          end
        end
        column 'Changes' do |c|
          if c.object_changes
            YAML.load(c.object_changes)
          end
        end
      end
    end
  end

  form partial: 'form'

  controller do
    def new
      if session[:booking]
        @booking = Booking.new(session[:booking])
      else
        @booking = Booking.new()
      end
      super
    end

    def update
      @booking.assign_attributes(booking_params)
      session[:booking] = booking_params

      @booking.company = Company.find_by(id: params[:select_company])
      @booking.location = Location.find_by(id: params[:location])

      # Get Services
      @booking.services.destroy_all
      ( @booking.services << Service.find(params[:base_service]) ) unless params[:base_service].blank?
      ( @booking.services << Service.find(params[:service_parameter]) ) unless params[:service_parameter].blank?
      ( @booking.services << Service.find(params[:booking][:extra_services_ids]) ) unless params[:booking][:extra_services_ids].blank?
      
      #Get Cleaners
      cleaners_search = params[:search].split(', ').map{ |c| c[/\(.*?\)/].gsub(/[()]/, "") }
      cleaners = cleaners_search.map{|id| Cleaner.find_by(id: id)}

      
      # When Customer is not created
      if params[:select_customer].empty?
        # Create Customer
        customer = Customer.new(
          first_name: params[:customer_firstname],
          last_name: params[:customer_lastname],
          email: params[:customer_email],
          phone: params[:customer_phone],
          role: 'customer'
        )
        customer.skip_password_validation = true
        if customer.save
          property = customer.properties.build(
            name: params[:property_name],
            address: params[:property_address],
            city: params[:property_city],
            state: params[:property_state],
            postal_code: params[:property_postal_code]
          )

          if property.save
            # Create Booking
            property.bookings << @booking
            @booking.cleaners = cleaners
            if @booking.save
              session[:booking] = nil
              @booking.calculate_fees
              redirect_to admin_bookings_path, notice: 'Booking updated sucessfuly'
            else
              redirect_to edit_admin_booking_path(@booking), alert: @booking.errors.full_messages
            end
          else
            redirect_to edit_admin_booking_path(@booking), alert: property.errors.full_messages
          end
        else
          redirect_to edit_admin_booking_path(@booking), alert: customer.errors.full_messages
        end
      else
        # When customer is selected
        # If property is not selected create property
        if params[:customer_properties]
          property = Property.find(params[:customer_properties])
          property.bookings << @booking
        else
          customer = Customer.find_by(id: params[:select_customer_id])
          property = customer.properties.build(
            name: params[:property_name],
            address: params[:property_address],
            city: params[:property_city],
            state: params[:property_state],
            postal_code: params[:property_postal_code]
          )
          customer.save
          property.bookings << @booking
        end

        unless @booking.cancelled
          @booking.cleaners = cleaners
        end

        if @booking.save
          @booking.calculate_fees
          session[:booking] = nil
          redirect_to admin_bookings_path, notice: 'Booking updated sucessfuly'
        else
          redirect_to edit_admin_booking_path(@booking), alert: @booking.errors.full_messages
        end
      end 
    end

    def create
      # Create Booking
      @booking = build_booking(booking_params)
      session[:booking] = booking_params
      
      @booking.company = Company.find_by(id: params[:select_company])
      @booking.location = Location.find_by(id: params[:location])

      # Get Services
      ( @booking.services << Service.find(params[:base_service]) ) unless params[:base_service].blank?
      ( @booking.services << Service.find(params[:service_parameter]) ) unless params[:service_parameter].blank?
      ( @booking.services << Service.find(params[:booking][:extra_services_ids]) ) unless params[:booking][:extra_services_ids].blank?
      
      #Get Cleaners
      cleaners_search = params[:search].split(', ').map{ |c| c[/\(.*?\)/].gsub(/[()]/, "") }
      cleaners = cleaners_search.map{|id| Cleaner.find_by(id: id)}

      
      # When Customer is not created
      if params[:select_customer].empty?
        # Create Customer
        customer = Customer.new(
          first_name: params[:customer_firstname],
          last_name: params[:customer_lastname],
          email: params[:customer_email],
          phone: params[:customer_phone],
          role: 'customer'
        )
        customer.skip_password_validation = true
        if customer.save
          property = customer.properties.build(
            name: params[:property_name],
            address: params[:property_address],
            city: params[:property_city],
            state: params[:property_state],
            postal_code: params[:property_postal_code]
          )

          if property.save
            # Create Booking
            property.bookings << @booking
            @booking.cleaners = cleaners
            if @booking.save
              session[:booking] = nil
              @booking.calculate_fees
              redirect_to admin_bookings_path, notice: 'Booking created sucessfuly'
            else
              redirect_to new_admin_booking_path, alert: @booking.errors.full_messages
            end
          else
            redirect_to new_admin_booking_path, alert: property.errors.full_messages
          end
        else
          redirect_to new_admin_booking_path, alert: customer.errors.full_messages
        end
      else
        # When customer is selected
        # If property is not selected create property
        if params[:customer_properties]
          property = Property.find(params[:customer_properties])
          property.bookings << @booking
        else
          customer = Customer.find_by(id: params[:select_customer_id])
          property = customer.properties.build(
            name: params[:property_name],
            address: params[:property_address],
            city: params[:property_city],
            state: params[:property_state],
            postal_code: params[:property_postal_code]
          )
          customer.save
          property.bookings << @booking
        end
        @booking.cleaners = cleaners

        if @booking.save
          @booking.calculate_fees
          session[:booking] = nil
          redirect_to admin_bookings_path, notice: 'Booking created sucessfuly'
        else
          redirect_to new_admin_booking_path, alert: @booking.errors.full_messages
        end
      end 
    end

    def booking_params
      booking_date = Time.zone.parse(params[:booking_date]).utc unless params[:booking_date].empty?
      location = Location.find_by(id: params[:location])
      {
        location: location,
        booking_date: booking_date,
        service_total:  params[:booking][:service_total],
        extras_total: params[:booking][:extras_total],
        sub_total: params[:booking][:sub_total],
        sales_tax: params[:booking][:sales_tax],
        final_amount: params[:booking][:final_amount],
        tip: params[:booking][:tip],
        payment_method: params[:booking][:payment_method],
        frecuency: params[:booking][:frecuency],
        discount_from_code:  params[:booking][:discount_from_code],
        discount_from_frecuency:  params[:booking][:discount_from_frecuency],
        discount_from_referral: params[:booking][:discount_from_referral],
        giftcard_amount_used: params[:booking][:giftcard_amount_used],
        staff_notes: params[:booking][:staff_notes],
        customer_notes:  params[:booking][:customer_notes],
        flexibility:params[:booking][:flexibility],
        entrance_method: params[:booking][:entrance_method],
        booking_assigned: params[:booking][:booking_assigned],
        cancelled: params[:booking][:cancelled],
        requested: params[:booking][:requested],
        hidden: params[:booking][:hidden]
      }
    end

    def build_booking(*args)
      Booking.new(
        location: args.first[:location],
        booking_date: args.first[:booking_date],
        service_total: args.first[:service_total], 
        extras_total: args.first[:extras_total],
        sub_total: args.first[:sub_total],
        sales_tax: args.first[:sales_tax],
        final_amount: args.first[:final_amount],
        tip: args.first[:tip],
        payment_method: args.first[:payment_method],
        frecuency: args.first[:frecuency],
        discount_from_code: args.first[:discount_from_code],
        discount_from_frecuency: args.first[:discount_from_frecuency],
        discount_from_referral: args.first[:discount_from_referral],
        giftcard_amount_used: args.first[:giftcard_amount_used],
        staff_notes: args.first[:staff_notes],
        customer_notes: args.first[:customer_notes],
        flexibility: args.first[:flexibility],
        entrance_method: args.first[:entrance_method],
        booking_assigned: args.first[:booking_assigned],
        cancelled:  args.first[:cancelled],
        requested:  args.first[:requested],
        hidden: args.first[:hidden]
      )
    end

    def set_booking
      @booking = Booking.find(params[:id])
    end
  end

  collection_action :archive_booking, method: :get do
    booking = Booking.find(params[:format])
    booking.update_column(:archived, true)
    booking.save!
    redirect_to admin_bookings_path, notice: 'Booking was successfully archived'
  end

  collection_action :unarchive_booking, method: :get do
    booking = Booking.find(params[:format])
    booking.update_column(:archived, false)
    booking.save!
    redirect_to admin_bookings_path, notice: 'Booking was successfully activated'
  end

  collection_action :get_customers, method: :get do
    @customers = Customer.all.map {|c| {:name => c.first_name + " " + c.last_name, :id => c.id}}
    respond_to do |format|
      format.json { render json: @customers }
    end
  end

  collection_action :get_properties, method: :get do
    @properties = Customer.find_by(id: params[:id]).properties
    respond_to do |format|
      format.json { render json: @properties }
    end
  end

  collection_action :get_cleaners, method: :get do
    @cleaners = Cleaner.all.map{|c| c.first_name + " " + c.last_name + ' (' + (c.id.to_s) + ')'}
    respond_to do |format|
      format.json { render json: @cleaners }
    end
  end

  collection_action :get_locations, method: :get do
    @locations = Company.find(params[:company_id]).locations
    render json: { locations: @locations }
  end

  collection_action :get_services, method: :get do
    @services = Company.find(params[:company_id]).services.where(service_type: 'base')
    render json: { services: @services }
  end

  collection_action :get_from_api, method: :get do
    start_date = params[:date].to_date
    end_date = params[:date].to_date
    GetBookingsFromL27.save_bookings(start_date, end_date)
    redirect_to admin_bookings_path, notice: 'Bookings were updated successfully'
  end

  collection_action :get_service_params, method: :get do
    unless params[:service_id].empty?
      @parameters = Service.find(params[:service_id]).parameters
      render json: { parameters: @parameters }
    else
      render json: { parameters: '' }
    end
  end

  collection_action :get_extra_services, method: :get do
    unless params[:service_id].empty?
      @extras = Service.find(params[:service_id]).children
      render json: { extras: @extras }
    else 
      render json: { extras: '' }
    end
  end

  ###### Custom Views ######

  # Random Bookings
  action_item 'random_bookings', only: :index do
    link_to 'Generate Random Bookings', random_bookings_admin_bookings_path
  end

  collection_action :random_bookings, method: :get do
    # Just render - no specific implementation
  end

  collection_action :random_bookings_process, method: :post do
    RandomBookingGenerator.generate_bookings(params["Generate Random Bookings"][:number_of_bookings].to_i, params["Generate Random Bookings"][:start_date], params["Generate Random Bookings"][:end_date]) 
    flash[:success] = "#{params["Generate Random Bookings"][:number_of_bookings]} bookings created"
    redirect_to admin_bookings_path
  end

  # Random Bookings
  action_item 'booking_calendar', only: :index do
    link_to 'Booking Calendar', booking_calendar_admin_bookings_path
  end

  collection_action :booking_calendar, method: :get do
    date = params[:date] ? params[:date].to_date : Date.today.to_date
    @bookings = Booking.where("DATE(booking_date) >= ? AND DATE(booking_date) < ? AND archived = ?", date.to_s, (date + 1.day).to_s, false).not_cancelled.group(:id, :company_id).order(:company_id, :booking_date)
    unless params[:filters].blank?
      filters =  JSON.parse(params[:filters])
      @bookings = @bookings.where("booking_assigned = ?", filters["claimed"]) unless filters["claimed"].blank?
      @bookings = @bookings.where(company_id: filters["company"]) unless filters["company"].blank?
    end
    
    if params[:date]
      render json: [companies: Company.all, bookings: @bookings.as_json(:include => [:cleaners, :company, :services, :property => {:include => :customer}]), claimed: @bookings.map{|b| b.booking_assigned? ? b.booking_assigned : nil }.count(true)]
    end
  end

  collection_action :a_booking_calendar, method: :get do
    date = params[:date] ? params[:date].to_date : Date.today.to_date
    bookings = Booking.where("company_id = ?", params[:id]).where("DATE(booking_date) >= ? AND DATE(booking_date) < ?", date.to_s, (date + 1.day).to_s).not_cancelled
    if params[:id]
      render json: { claimed: bookings.claimed.count(true), total: bookings.count }
    end
  end

  collection_action :assign_booking, method: :post do
    render json: { status: "error", message:"There was an error trying to assign booking" }, status: :unprocessable_entity unless (params[:data] || (params[:data][:cleaners] && params[:data][:booking]))
    if params[:data]
      if params[:data][:cleaners] && params[:data][:booking]
        booking = Booking.find(params[:data][:booking])
        cleaners = booking.cleaners << Cleaner.find(params[:data][:cleaners])
        render json: { status: "error", message:"There was an error trying to assign booking" }, status: :unprocessable_entity unless cleaners
        booking.update_attribute(:booking_assigned, true)
        render json: { status: "success", cleaners: cleaners.pluck(:full_name, :id).map{|e| "#{e[0]}(#{e[1]})"}.join(", ") }, status: :ok
      end
    end
  end

  collection_action :hide_booking, method: :post do
    render json: { status: "error", message:"There was an error trying to assign booking" }, status: :unprocessable_entity unless params[:id]
    booking = Booking.find(params[:id])
    booking.cleaners.destroy_all
    booking.update_attributes(booking_assigned: false, completed: false, requested: false, cancelled: false, hidden: true)
    render json: { status: "success" }, status: :ok
  end
end
