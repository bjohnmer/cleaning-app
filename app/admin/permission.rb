ActiveAdmin.register Permission do
  permit_params :name, :hour, :minutes

  form do |f|
    f.inputs 'Permission' do
      f.input :name
      f.input :hour, as: :select, collection: ['00', '01', '02', '03', '04', '05', '06','07', '08', '09', '10', 
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'], include_blank: false  
      f.input :minutes, as: :select, collection: ['00', '10', '20', '30', '40', '50', '60'], include_blank: false
    end
    f.actions
  end
end
