ActiveAdmin.register Area do
  permit_params :name, zipcodes_attributes: [ :number ]

  show do
    attributes_table do
      row :name
    end
    panel "Zipcodes" do
      zipcodes = Area.find(params['id']).zipcodes
      table_for zipcodes do
        column :number
      end
    end
  end

  form do |f|
    f.inputs 'Area' do
      f.input :name
      f.has_many :zipcodes, heading: 'Zipcodes' , new_record: "Add a New Zipcode" do |zipcode|
        zipcode.input :number
      end
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :name
    column "Zip Codes" do |area|
      area.zipcodes.map{|code| code.number}
    end
    actions
  end
end
