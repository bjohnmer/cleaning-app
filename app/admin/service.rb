ActiveAdmin.register Service do
  permit_params :name, :description, :price, :service_type, :quantity_based, :staff_only, 
    :ancestry, :parent_id, :company_id, parameters_attributes: [ :name, :description, :price, 
    :service_type, :quantity_based, :staff_only, :company_id ]
  
  scope :active, default: true
  scope :archived

  index do
    selectable_column
    id_column
    column :name
    column :price
    column :service_type
    column :company
    column :parent do |p|
      if p.parent_id
        p.parent
      else
        if p.service_type == 'parameter'
          Service.find_by(id: p.service_id)
        else
          'Base Service'
        end
      end
    end
    # actions
    column "" do |resource|
      links = ''.html_safe
      links += link_to 'View', resource_path(resource), class: "member_link show_link"
      links += link_to 'Edit', edit_resource_path(resource), class: "member_link edit_link"
      if resource.archived?
        links += link_to 'Unarchived', unarchive_service_admin_services_path(resource), confirm: 'Are you sure that you want to unarchive the Service', class: "member_link archived_link"
        links += link_to 'Delete', resource_path(resource), method: :delete, confirm: I18n.t('active_admin.delete_confirmation'), class: "member_link delete_link"
      else
        links += link_to 'Archive', archive_service_admin_services_path(resource), confirm: 'Are you sure that you want to Archive the Service', class: "member_link archived_link"
      end
      links
    end
  end

  show do
    attributes_table do
      row :name
      row :description
      row :price
      row :service_type
      row :created_at
      row :updated_at
      row :quantity_based
      row :staff_only
      row :company
    end
    if service.service_type == 'base'
      panel "Pricing Parameters" do
        parameters = service.parameters
        table_for parameters do
          column :name
          column :price
          column :description
          column :service_type
          column :quantity_based
          column :quantity_based
          column :staff_only
        end
      end
      panel "Extra Services" do
        services = service.children
        table_for services do
          column :name
          column :price
          column :description
          column :service_type
          column :quantity_based
          column :quantity_based
          column :staff_only
        end
      end
    else
      panel "Parent Service" do
        parent = service.parent if service.parent
        parent = Service.find(service.service_id) if service.service_id
        table_for parent do
          column :name
          column :price
          column :description
          column :service_type
          column :quantity_based
          column :quantity_based
          column :staff_only
          column :company
        end
      end
    end
  end



  form do |f|
    f.inputs 'Service' do
      f.input :name
      f.input :description
      f.input :price
      f.input :service_type, as: :select, collection: (["base", "add_on"])
      f.input :quantity_based
      f.input :staff_only
      f.input :company
      f.input :parent_id, :as => :select, collection: Service.select {|s| s.service_type == 'base'}.map{|s| ["Company: #{s.company.name}. Service: #{s.name}", s.id]}
      f.has_many :parameters, heading: 'Pricing Parameters' , new_record: "Add New Pricing Parameter" do |parameter|
        parameter.input :name
        parameter.input :description
        parameter.input :price
        parameter.input :service_type, as: :select, collection: (["parameter"]), include_blank: false
        parameter.input :quantity_based
        parameter.input :staff_only
        parameter.input :company
      end
    end
    f.actions
  end

  collection_action :unarchive_service, method: :get do
    service = Service.find(params[:format])
    service.update_column(:archived, false)
    service.save!
    redirect_to admin_services_path, notice: 'Service was successfully activated'
  end

  collection_action :archive_service, method: :get do
    service = Service.find(params[:format])
    service.update_column(:archived, true)
    service.save!
    redirect_to admin_services_path, notice: 'Service was successfully archived'
  end
end

