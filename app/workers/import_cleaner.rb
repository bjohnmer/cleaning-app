class ImportCleaner
  @queue = :import_cleaner
  
  def self.perform(file, companies)
    Cleaner.import_cleaners(file, companies)
  end
end