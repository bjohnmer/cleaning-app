class ImportCustomer
  @queue = :import_customer
  
  def self.perform(file)
    Rails.logger.info('Starting import process')
    Rails.logger.info(file)
    Customer.import_customer(file)
  end
end