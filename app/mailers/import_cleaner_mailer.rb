class ImportCleanerMailer < ApplicationMailer
  default from: "info@servuspros.com"

  def send_email(user, created, errors)
  	if ENV['ROOT_URL'] == 'http://servuspros.com' || Rails.env.local?
	    @user = user
	    @created = created
	    @errors = errors
	    email = @user.email
      mail(to: email, subject: 'Import Cleaner Email')
		else
			@user = user
	    @created = created
	    @errors = errors
	    mail(to: 'theservusapp@gmail.com', subject: 'Import Cleaner Email')
		end
  end
end
