class ApplicationMailer < ActionMailer::Base
  default from: "info@servuspros.com"
  layout 'mailer'
end
