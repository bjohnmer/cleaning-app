class BookingMailer < ApplicationMailer
  default from: "info@servuspros.com"

  def notify_cancelation(user, booking)
    @user = user
    @booking = booking
    if ENV['ROOT_URL'] == 'http://servuspros.com' || Rails.env.local?
      email = @user.email
      mail(to: email, subject: 'Your Planned Booking was cancelled')
    else
      mail(to: 'theservusapp@gmail.com', subject: 'Your Planned Booking was cancelled')
    end
  end

  def notify_cancelation_to_managers(user, booking)
    @user = user
    @booking = booking 
    if ENV['ROOT_URL'] == 'http://servuspros.com' || Rails.env.local?
      email = @user.email
      mail(to: email, subject: 'Important! '+@booking.property.customer.full_name+' cancelled booking')
    else
      mail(to: 'theservusapp@gmail.com', subject: 'Important! '+@booking.property.customer.full_name+' cancelled booking')
    end
  end

  def notify_claimed_booking_to_managers(manager, booking, cleaner)
    @manager = manager
    @cleaner = cleaner
    @booking = booking
    if ENV['ROOT_URL'] == 'http://servuspros.com' || Rails.env.local?
      email = @manager.email
      mail(to: email, subject: 'Important! ' + @cleaner.full_name + 'has accepted a booking.')
    else
      mail(to: 'theservusapp@gmail.com',  subject: 'Important! ' +@cleaner.full_name+ 'has accepted a booking.')
    end
  end

  def email_today_bookings(cleaner, bookings)
    @cleaner = cleaner
    @bookings = bookings
    if @cleaner.send_emails
      mail(to: @cleaner.email, subject: 'Here’s ' +@cleaner.full_name+ ' schedule for ' + Time.current.strftime("%A %d, %B, %Y"))
    else
      mail(to: 'theservusapp@gmail.com', subject: 'Here’s ' +@cleaner.full_name+ ' schedule for ' + Time.current.strftime("%A %d, %B, %Y"))
    end
  end

  def email_tomorrow_bookings(cleaner, bookings)
    @cleaner = cleaner
    @bookings = bookings 
    if @cleaner.send_emails
      mail(to: @cleaner.email, subject: 'Here’s ' +@cleaner.full_name+ ' schedule for ' + (Time.current + 1.day).strftime("%A %d, %B, %Y"))
    else
      mail(to: 'theservusapp@gmail.com',subject: 'Here’s ' +@cleaner.full_name+ ' schedule for ' + (Time.current + 1.day).strftime("%A %d, %B, %Y"))
    end
  end

  def email_customer_request(cleaner, booking)
    @cleaner = cleaner
    @booking = booking
    if @cleaner.send_emails
      mail(to: @cleaner.email, subject: 'You have a new requested booking')
    else
      mail(to: 'theservusapp@gmail.com', subject: 'You have a new requested booking')
    end
  end

  #Managers Mailing
  def notify_booking_not_accepted_to_managers(manager, booking)
    @manager = manager
    @booking = booking
    mail(to: @manager.email, subject: 'Important! Booking not accepted on time')
  end

  def notify_declined_booking_to_managers(manager, booking, cleaner)
    @manager = manager
    @cleaner = cleaner
    @booking = booking
    if ENV['ROOT_URL'] == 'http://servuspros.com' || Rails.env.local?
      email = @manager.email
      mail(to: email, subject: 'Important! ' +@cleaner.full_name+ 'has declined a requested booking.')
    else
      mail(to: 'theservusapp@gmail.com',  subject: 'Important! ' +@cleaner.full_name+ 'has declined a requested booking.')
    end
  end
end
