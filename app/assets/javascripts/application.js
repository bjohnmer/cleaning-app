// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
// require_tree .
//= require bootstrap-sprockets
//= require bootstrap
//= require moment
//= require fullcalendar

$(document).ready(function() {
  var selected = new Array();
  var index = 0;
  var initialLocaleCode = 'es';
  var today = moment().format('YYYY-MM-DD');

  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listMonth'
    },
    defaultTimedEventDuration: '01:00:00',
    defaultDate: today,
    locale: initialLocaleCode,
    defaultView: 'agendaDay',
    buttonIcons: false,
    weekNumbers: true,
    navLinks: true,
    editable: true,
    eventLimit: true,
    eventSources: [
      {
        events: function(start, end, timezone, callback) {
          $.ajax({
              url: '/cleaners/calendar/bookings',
              dataType: 'json',
              success: function(bookings) {
                var events = [];
                $.each(bookings, function(index, val) {
                  $.each(val, function(index, val) {
                    events.push({
                        id: val.id,
                        title: val.title,
                        start: val.start
                    });
                  });
                });
                callback(events);
              }
          });
        },
        color: 'black',
        textColor: 'yellow'
      }
    ],
    eventClick: function(calEvent, jsEvent, view) {
      console.log(calEvent)
      $('#BookingInfo').modal({
        backdrop: 'static',
        keyboard: false
      });
      $.ajax({
          url: '/cleaners/calendar/booking',
          dataType: 'json',
          data: {id: calEvent.id },
          success: function(booking) {
            $("#booking").html(booking.booking_html);
          }
      });
      return false;
    }
  });
});
