module BookingsHelper

  def base_service(services)
    return services.select { |service| service.service_type == 'base' }.first
  end

  def params_service(services)
    service = services.select { |service| service.service_type == 'parameter' }.first
    return service ? service.name : 'No service parameter'
  end

  def extra_services(services)
    return services.select { |service| service.service_type == 'add_on' }
  end

  def show_address(booking, user)
    Time.current.utc.in_time_zone(user.timezone) >= (booking.booking_date.localtime - 1.day).to_time.change(hour: Config.fetch("show_adress_time").to_i).localtime ? true : false
  end

  def show_earnings_percetage(user, booking_amount)
    earnings = user.percentage_of_booking
    return earnings ? (((earnings * booking_amount ) / 100 ).to_f.round(2)) : 0
  end

  def show_area(zipcode)
    zipcode = zipcode.scan(/(.{1,5})/).flatten.first
    zipcode = Zipcode.find_by_number(zipcode)
    zipcode ? zipcode.area.name : 'No area found'
  end

  def show_time_left_to_claim(date, user)
    time = date.to_time.utc.in_time_zone(user.timezone) - Time.current.in_time_zone(user.timezone)
    time_left = (time / 60) - (Config.fetch('requested_booking_limit').to_i * 60) - (24 * 60)
    days = (time_left / (24 * 60)).to_i
    hours = (time_left / 60 - (days * 24)).to_i
    minutes = (time_left - (days * 24 * 60) - (hours * 60)).to_i
    return "#{days} days, #{hours} hours, #{minutes} minutes"
  end

  def show_cut_of_time
    start_time = Config.fetch('requested_booking_limit').to_i > 12 ? (Config.fetch('requested_booking_limit').to_i ) - 12 : (Config.fetch('requested_booking_limit').to_i )
    end_time = Config.fetch('cut_of_time').to_i > 12 ? Config.fetch('cut_of_time').to_i - 12 : Config.fetch('cut_of_time').to_i
    start_am_pm = Config.fetch('requested_booking_limit').to_i > 12 ? 'PM' : 'AM'
    end_am_pm = Config.fetch('cut_of_time').to_i > 12 ? 'PM' : 'AM'
    return "#{start_time} #{start_am_pm} today and #{end_time} #{end_am_pm} tomorrow"
  end
end
