class PagesController < ApplicationController
  def index
    if current_user.actable_type == "Cleaner"
      redirect_to cleaners_dashboard_path
    end
  end
end
