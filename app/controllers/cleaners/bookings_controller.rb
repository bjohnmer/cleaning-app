class Cleaners::BookingsController < ApplicationController
  before_action :set_booking, only: [:update, :show, :cancel_booking, :accept_booking, :decline_booking, :get_booking]

  def index
    @cleaner = Cleaner.find(current_user.actable_id)
    @bookings = Kaminari.paginate_array(@cleaner.available_bookings).page(params[:page]).per(20)
    if params[:from] && params[:to]
      time = Time.current + 1.day
      @bookings = Kaminari.paginate_array(@cleaner.available_bookings(from: time.change( hour: params['from']), to: time.change( hour: params['to']))).page(params[:page]).per(20)
    end
  end

  def show
    @cleaner = Cleaner.find(current_user.actable_id)
  end

  def update
    # Check if booking has not been taken yet
    cleaner = Cleaner.find(current_user.actable_id)
    if @booking.booking_assigned
      flash[:warning] = "This Booking has already been taken."
      redirect_to cleaners_bookings_path
    else
      if cleaner.can_claim_booking?
        cleaner.pick_booking(@booking)
        if @booking.save
          flash[:success] = "Booking Claimed Succesfully."
          redirect_to cleaners_bookings_path
        else
          flash[:success] = "Error claiming this booking."
          redirect_to cleaners_bookings_path
        end
      else
        flash[:warning] = "You have reached your max amount of bookings for tomorrow."
        redirect_to cleaners_bookings_path
      end
    end
  end

  def cancel_booking
    @booking.cancel_booking
    if @booking.save
      flash[:success] = "You have cancel the booking successfully."
      redirect_to cleaners_dashboard_path
    end
  end

  def previous
    @cleaner = Cleaner.find(current_user.actable_id)
    @bookings = Kaminari.paginate_array(@cleaner.get_previous_bookings).page(params[:page]).per(20)
  end

  def calendar
  end

  def get_bookings
    @cleaner = Cleaner.find(current_user.actable_id)
    @bookings = @cleaner.bookings
  end

  def get_booking
    @cleaner = Cleaner.find(current_user.actable_id)
    booking_html = render_to_string(:template => 'cleaners/bookings/_claim_booking.html.erb', :layout => false, :locals => { :booking => @booking })
    render :json => { :booking_html => booking_html }, :status => :ok
  end

  def email_bookings
    unless params['bookings'] == nil
      if params['date'] == 'today'
        Cleaner.find(current_user.actable_id).email_today_bookings(params['bookings'])
      else
        Cleaner.find(current_user.actable_id).email_tomorrow_bookings(params['bookings'])
      end
    end
    flash[:success] = "You bookings were emailed succesfully. Please review your email."
    redirect_to cleaners_dashboard_path
  end

  def accept_booking
    @booking.accept_request(current_user)
    if @booking.save
      flash[:success] = "You have succesfully accepted the customer request."
      redirect_to cleaners_dashboard_path
    end
  end

  def decline_booking
    @booking.decline_request(current_user)
    if @booking.save
      flash[:success] = "You have declined the customer request."
      redirect_to cleaners_dashboard_path
    end
  end

  private
    
  def set_booking
    @booking = Booking.find_by(id: params[:id])
  end
end
