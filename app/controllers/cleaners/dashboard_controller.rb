class Cleaners::DashboardController < ApplicationController
  
  def index 
    @cleaner = Cleaner.find(current_user.actable_id)
    @bookings = @cleaner.get_todays_bookings
    @future_bookings = @cleaner.get_assigned_bookings_for_tomorrow
  end
end