class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:new_booking, :cancel_booking, :complete_booking, :update_booking]
  before_action :authenticate_user!, :except => [:new_booking, :cancel_booking, :complete_booking, :update_booking]
  before_action :cors_set_access_control_headers

  def new_booking
    information = JSON.parse(request.raw_post)
    # Search For Company
    company = Company.find_by(name: information['company'])
    if company
      # Create of Find Customer
      customer = create_customer(information['first_name'], information['last_name'], information['phone'], information['email'])

      # Check if the customer has this property already
      property = create_properties(information['address'], information['city'], information['state'], information['zip'], customer)
      add_property_to_customer(customer, property)

      if customer.save!
        location = create_location(information['sales_tax'], information['city'], information['state'])
        if location

          booking = create_booking(location, information, property, company)      
          #Base Service
          base_service = get_base_service(information['service'], information['service_price'], company)
          booking.services << base_service
          pricing_parameter = get_parameter_service(information['pricing_parameters'], information['extra_price'], information['subtotal'], base_service, company,booking)
          get_extra_services(information['extras'], information['extra_price'], booking, base_service, company)

          #Get Team
          assign_teams(information['assigned_teams'], booking)

          if booking.save!
            render json: { message: "Booking Created Sucessfully" }, status: 200
          else
            notify_failure_message("Error on creating Booking")
            render json: { message: 'Error on creating Booking' }, status: 422
          end
        else
          notify_failure_message("Error on creating Booking")
          render json: { message: 'Location does not exist.' }, status: 404
        end
      else
        notify_failure_message("Error on creating Booking")
        render json: { message: 'Error on creating Customer' }, status: 422
      end
    else
      notify_failure_message("Error on creating Booking")
      render json: { message: 'Company does not exist.' }, status: 404
    end  
  end

  def cancel_booking
    information = JSON.parse(request.raw_post)
    booking = Booking.find_by(launch_27_id: information['launch_27_id'])

    if booking
      booking.cancelled = true
      booking.cancelled_reason = information['cancelled_reason']
      if booking.save
        render json: { message: 'Booking updated' }, status: 200
      else
        notify_failure_message("Error cancelling booking")
        render json: { message: 'Error updating booking' }, status: 422
      end
    else
      notify_failure_message("Error cancelling booking")
      render json: { message: 'Booking does not exist.' }, status: 404
    end
  end

  def update_booking
    information = JSON.parse(request.raw_post)

    # Search For Company
    company = Company.find_by(name: information['company'])
    if company
      location = create_location(information['sales_tax'], information['city'], information['state'])

      if location
        booking = Booking.find_by(launch_27_id: information['launch_27_id'])
        # Check if last name exits, if not set it up to not last name
        
        # Create of Find Customer
        customer = create_customer(information['first_name'], information['last_name'], information['phone'], information['email'])
        property = create_properties(information['address'], information['city'], information['state'], information['zip'], customer)
        add_property_to_customer(customer, property)


        if customer.save!
          if booking
            booking_data = { 
              booking.id => { 
                extras_total: information['extra_price'].tr('$', '').to_f,
                booking_date: DateTime.parse( [ information['service_date'], information['service_time'] ].join(' ')),
                service_total: information['final_price'].tr('$', '').to_f,
                sub_total: information['subtotal'].tr('$', '').to_f,
                sales_tax: information['sales_tax'].tr('$', '').to_f,
                final_amount: information['charge_amount'].tr('$', '').to_f,
                tip: information['tip'].tr('$', '').to_f,
                payment_method: information['payment_methods'],
                frecuency: information['frecuency'],
                giftcard_amount_used: information['giftcard_amount'],
                customer_notes: information['customer_notes'],
                frecuency: information['frequency'],
              }
            }

            # Erase all related current Services
            booking.services.destroy_all
            #Base Service 
            base_service = get_base_service(information['service'], information['service_price'], company)
            booking.services << base_service
          
            # Service Parameter
            # Get pricing_parameter (Service Subtoal - Extra Total - Base Price)
            pricing_parameter = get_parameter_service(information['pricing_parameters'], information['extra_price'], information['subtotal'], base_service, company,booking)
            get_extra_services(information['extras'], information['extra_price'], booking, base_service, company)

            #Update Team
            booking.cleaners.destroy_all
            
            #Get Team
            assign_teams(information['assigned_teams'], booking)

            #Reinstate a booking if it was cancelled
            if booking.cancelled == true
              booking.update_column(:cancelled, false)
            end

            if Booking.update(booking_data.keys, booking_data.values)
              render json: { message: 'Booking updated' }, status: 200
            else
              render json: { message: 'Error on update Booking' }, status: 422
            end
          else
            
            booking = create_booking(location, information, property, company)  

            #Base Service
            base_service = get_base_service(information['service'], information['service_price'], company)
            booking.services << base_service
          

            # Service Parameter
            # Get pricing_parameter (Service Subtoal - Extra Total - Base Price)
            pricing_parameter = get_parameter_service(information['pricing_parameters'], information['extra_price'], information['subtotal'], base_service, company,booking)
            get_extra_services(information['extras'], information['extra_price'], booking, base_service, company)
            
            #Get Team
            assign_teams(information['assigned_teams'], booking)
            
            if booking.save!
              render json: { message: "Booking Created Sucessfully" }, status: 200
            else
              notify_failure_message("Error on updating a Booking")
              render json: { message: 'Error on creating Booking' }, status: 422
            end
          end
        else
          notify_failure_message("Error on updating a Booking")
          render json: { message: 'Error on creating Customer' }, status: 422
        end 
      else
        notify_failure_message("Error on updating a Booking")
        render json: { message: 'Location does not exist.' }, status: 404
      end
    else
      notify_failure_message("Error on updating a Booking")
      render json: { message: 'Company does not exist.' }, status: 404
    end
  end

  def complete_booking
    information = JSON.parse(request.raw_post)
    booking = Booking.find_by(launch_27_id: information['launch_27_id'])
    if booking
      booking.completed = true
      if booking.save
        render json: { message: 'Booking updated' }, status: 200
      else
        notify_failure_message("Error on completing a Booking")
        render json: { message: 'Error updating booking' }, status: 422
      end
    else
      notify_failure_message("Error on completing a Booking")
      render json: { message: 'Booking does not exist.' }, status: 404
    end
  end

  def notify_failure_message(message)
    notifier = Slack::Notifier.new "https://hooks.slack.com/services/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" do
      defaults channel: "#webhooks_notifier",
              username: "Webhook notifier"
    end
    notifier.ping(message)
  end

  private
  def cors_set_access_control_headers
    unless request.headers['HTTP_ZAPPIER'] == ENV['ZAPIER_WEBHOOKS_PASSWORD']
      Rails.logger.info('Unauthorized Request');
      render json: { message: 'Unauthorized Request' }, status: :unauthorized
    end
  end

  def create_customer(f_name, l_name, phone, email)
    name = set_name(f_name, l_name)
    first_name = name[0]
    last_name = name[1]
    customer = Customer.create_with(
      first_name: first_name,
      last_name: last_name,
      phone: phone,
      role: 'customer',
    ).find_or_create_by(email: email)
    customer.skip_password_validation = true
    customer
  end

  def set_name(f_name, l_name)
    first_name = f_name
    last_name = l_name
    if l_name == ""
      first_name = f_name.split(' ').first
      last_name = f_name.split(' ').second
    end
    last_name = 'No last name' if last_name == nil
    return first_name, last_name
  end

  def create_properties(address, city, state, zip, customer)
    property = Property.create_with(
      state: state,
      postal_code: zip, 
      city: city
    ).find_or_create_by(address: address, customer: customer)
  end

  def add_property_to_customer(customer, property)
    unless customer.properties.pluck(:address).include?(property.address)
      customer.properties << property
    end
  end

  def create_location(tax, city, state)
    location = Location.create_with(tax: tax.tr('$', '').to_f).find_or_create_by(name: city)
    # if location does not exists find by state 
    unless location
      if state == 'NY'
        location = Location.find_by(name: 'New York')
      elsif state == 'MA'
        location = Location.find_by(name: 'Boston')
      elsif state == 'NJ'
        location = Location.find_by(name: 'New Jersey')
      elsif state == 'IL'
        location = Location.find_by(name: 'Chicago')
      else
        location = Location.find_by(name: 'New York')
      end
    end
    location
  end

  def create_booking(location, information, property, company)
    discount = information['discount'] == '' || information['discount'] == nil ? 0 : information['discount'].tr('$', '').to_f
    booking = Booking.create_with(
      location: location,
      booking_date: DateTime.parse([information['service_date'], information['service_time'] ].join(' ')),
      service_total: information['final_price'].tr('$', '').to_f,
      extras_total: information['extra_price'].tr('$', '').to_f,
      sub_total: information['subtotal'].tr('$', '').to_f,
      sales_tax: information['sales_tax'].tr('$', '').to_f,
      final_amount: information['charge_amount'].tr('$', '').to_f,
      tip: information['tip'].tr('$', '').to_f,
      payment_method: information['payment_methods'],
      frecuency: information['frequency'],
      giftcard_amount_used: information['giftcard_amount'],
      customer_notes: information['customer_notes'],
      discount_from_frecuency: discount,
      property: property,
      company: company,
    ).find_or_create_by(launch_27_id: information['launch_27_id'])
    #Reinstate a booking if it was cancelled
    if booking.cancelled == true
      booking.cancelled = false
    end
    booking
  end

  def get_base_service(service, price, company)
    if service.scan(/Hourly/).first == 'Hourly'  
      hourly_services = service.scan(/([a-zA-Z\s\(\)\-]{1,})\(([^\)]+)\)/).flatten.map(&:strip).reject { |c| c.empty? }
      hourly_name = hourly_services.first
      hourly_services.select! {|s| s != 'Hourly Service'  }
      if hourly_services.count > 1
        hourly_price = hourly_services.second.split('x ').second.split(' ').first.to_f
      else
        hourly_price = hourly_services.first.split('x ').second.split(' ').first.to_f
      end
      hourly_price = price.tr('$', '').to_f / hourly_price
      base_service = company.services.create_with(service_type: 'base').find_or_create_by(name: hourly_name, price: hourly_price )
    else
      base_service = service.scan(/([a-zA-Z\s\(\)\-]{3,})[\s\+\$]+([0-9]+)/)
      unless base_service.empty?
        base_service = base_service.map{|s| Hash[*s.map(&:strip)] }.first
        base_service = company.services.create_with(service_type: 'base').find_or_create_by(name: base_service.keys.first, price: base_service.values.first )
      else
        base_service = company.services.create_with(service_type: 'base').find_or_create_by(name: service, price: price.tr('$', '').to_f, )
      end
    end
    base_service
  end

  def get_parameter_service(parameter, price, subtotal, base_service, company, booking)
    if parameter
      parameter_cost =  subtotal.tr('$', '').to_f - price.tr('$', '').to_f - base_service.price
      pricing_parameter = parameter.split("x").map(&:strip).join(' ') 
      pricing_parameter = company.services.create_with(service_type: 'parameter').find_or_create_by(name: pricing_parameter, price: parameter_cost )
      base_service.parameters << pricing_parameter
      booking.services << pricing_parameter
    end
  end
  
  def get_extra_services(extras, extra_price, booking, base_service, company)
    if extras
      extra_services = extras.split('<br/>')
      # We can only calculate the price of the extra service by dividing the extras for subtotal
      add_on_cost = extra_price.tr('$', '').to_f / extra_services.count
      extra_services.each do |extra|
        # If the service includes ' x ' means that it should be hourly service
        booking.services << company.services.create_with(service_type: 'add_on').find_or_create_by(name: extra, price: add_on_cost, ancestry: base_service.id )
      end
    end
  end

  def assign_teams(team, booking)
    if team
      team = team.split(': ').last
      team = team.scan(/([^,]+)/).flatten.map(&:strip)
      team.each do |t|
        cleaner = Cleaner.find_by(full_name: t)
        booking.cleaners << cleaner if cleaner
      end
      booking.requested = true
    end
  end
end
