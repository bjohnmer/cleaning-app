class CleanersController < ApplicationController
	before_action :set_cleaner, only: [:edit, :update]

  def edit
  end

  def update
    if params[:cleaner][:password].blank? && params[:cleaner][:password_confirmation].blank?
      params[:cleaner].delete("password")
      params[:cleaner].delete("password_confirmation")
    end
    respond_to do |format|
      if @cleaner.update(cleaner_params)
        flash[:notice] = 'Cleaner was updated succesfully'
        format.html { redirect_to edit_cleaner_path(@cleaner) }
      else
        flash[:error] = @cleaner.errors
        format.html { render :edit }
      end
    end
  end

  def back_to_admin
    cleaner_user = current_user
    sign_out(cleaner_user)
    session["cleaner_session_id"] = nil
    admin = User.find(session["admin_session_id"])
    sign_in(:user, admin)
    redirect_to admin_dashboard_path
  end

  def destroy_admin_session
    @current_user = Cleaner.find(session["cleaner_session_id"])
    sign_out_and_redirect(@current_user)
  end

  private
    
    def set_cleaner
      @cleaner = Cleaner.find_by(id: current_user.actable_id)
    end

    def cleaner_params
      params.require(:cleaner).permit(:first_name, :last_name, :email, :password, :password_confirmation, :phone_number, :address, :postal_code, :state, :city, :max_bookings, :rating, :timezone, :role )
    end
end