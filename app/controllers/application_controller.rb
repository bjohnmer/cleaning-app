class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  before_action :set_paper_trail_whodunnit
  rescue_from ActionController::InvalidAuthenticityToken, with: :redirect_to_referer_or_path

  def redirect_to_referer_or_path
    flash[:notice] = "Please try again."
    redirect_to root_url
  end
  
  def after_sign_in_path_for(resource)
    if resource.actable_type == "Cleaner"
      return cleaners_dashboard_path
    elsif resource.actable_type == "Manager"
      return admin_root_path
    end
  end

  def authenticate_user!
    if session["cleaner_session_id"]
      @current_user = Cleaner.find(session["cleaner_session_id"])
    else
      super
    end
  end
end
