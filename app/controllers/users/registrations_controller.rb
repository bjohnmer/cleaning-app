class Users::RegistrationsController < Devise::RegistrationsController

  def create_cleaner
    @cleaner = Cleaner.new(cleaner_params)
    respond_to do |format|
      if @cleaner.save
        flash[:notice] = 'Cleaner was created, now you can sign in'
        format.html { redirect_to new_user_session_path }
      else
        flash[:error] = @cleaner.errors
        format.html { redirect_to new_user_session_path }
      end
    end
  end
  
  private
    def cleaner_params
      params.require(:cleaner).permit(:first_name, :last_name, :email, :password, :password_confirmation, :phone_number, :address, :postal_code, :state, :city, :max_bookings, :rating, :timezone, :role )
    end

  protected
    def after_sign_up_path_for(resource)
      signed_in_root_path(resource)
    end

    def after_update_path_for(resource)
      signed_in_root_path(resource)
    end

    def sign_up_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
    end

    def update_resource(resource, params)
      resource.update_without_password(params)
    end

    def account_update_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :current_password)
    end
end