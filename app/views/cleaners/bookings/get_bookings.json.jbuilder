json.bookings(@bookings) do |b|
  json.id b.id
  json.start b.booking_date
  json.end b.booking_date + 1.hour
  json.title "#{b.property.customer.full_name} #{b.property.postal_code}"
end