Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root 'pages#index'
    # get '/cleaners/sign_in', to: 'devise/sessions#new'
  devise_for :users, :controllers => { registrations: 'users/registrations', sessions: 'users/sessions' }, path: '', path_names: { sign_in: 'cleaners/sign_in', sign_out: '/users/sign_out' }
  devise_scope :user do
    get '/users/sign_out', to: 'devise/sessions#destroy'
    post '/register/cleaner', to: 'users/registrations#create_cleaner'
  end

  mount Resque::Server, at: "/resque"
  # For running resque queues it must be redis server running
  # Run this line in terminal
  # QUEUE=* rake resque:work

  namespace :cleaners do
    get "/dashboard", to: "dashboard#index"
    get '/calendar', to: "bookings#calendar"
    get "/calendar/booking", to: "bookings#get_booking"
    get "/calendar/bookings", to: "bookings#get_bookings"
    resources :bookings do 
      post 'cancel_booking', to: "bookings#cancel_booking", on: :member
      post 'accept_booking', to: "bookings#accept_booking", on: :member
      post 'decline_booking', to: "bookings#decline_booking", on: :member
      get 'email_booking', to: "bookings#email_bookings", on: :collection
      get 'previous', to: "bookings#previous",  on: :collection
    end
  end

  resources :cleaners do
    get "/back_to_admin", to: "cleaners#back_to_admin", as: "back_to_admin", on: :collection
    get "/destroy_admin_session", to: "cleaners#destroy_admin_session", as: "destroy_admin_session", on: :collection
  end

  scope '/webhooks' do
    post 'new_booking', to: 'webhooks#new_booking'
    post 'cancel_booking', to: 'webhooks#cancel_booking'
    post 'complete_booking', to: 'webhooks#complete_booking'
    post 'update_booking', to: 'webhooks#update_booking'
  end
end
