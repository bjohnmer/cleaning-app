#config/initializers/redis.rb
require "redis"

# if url = ENV['REDISTOGO_URL']
#   uri = URI.parse(url)
#   REDIS = Redis.new(:url => uri)
#   Resque.redis = REDIS
# elsif Rails.env.development? or Rails.env.test?  # or check ENV['RACK_ENV']
  $redis = Redis.new(:host => ENV["REDIS_PORT_6379_TCP_ADDR"], :port => ENV["REDIS_PORT_6379_TCP_PORT"], :db => 0)
  Resque.redis = $redis
  $redis.flushdb if Rails.env == "test"
# else
  # supposed to have a server
  # raise "Missing redis server - please set REDISTOGO_URL env var"
# end
