set :branch, 'master'
set :rails_env, 'production'

# Extended Server Syntax
#:w ======================
server '174.138.86.90', user: 'deploy', roles: %w{web app db resque_worker} # Digital Ocean
