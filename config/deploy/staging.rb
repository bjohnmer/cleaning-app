set :branch, 'staging'
set :rails_env, 'production'

# Extended Server Syntax
# ======================
server '165.227.112.59', user: 'deploy', roles: %w{web app db resque_worker} # Digital Ocean
