require 'rails_helper'

RSpec.describe Booking, type: :model do

  describe "validations" do
    it { should validate_presence_of(:booking_date) }
  end
  
  describe "relations" do
    it { should belong_to(:property) }
    it { should belong_to(:company) }
    it { should belong_to(:location) }
    it { should have_many(:cleaner_bookings) }
    it { should have_many(:cleaners).through(:cleaner_bookings) }
    it { should have_many(:booking_services) }
    it { should have_many(:services).through(:booking_services) }
  end

  before(:each) do
    @customer = FactoryBot.create(:customer, first_name: 'Henry', last_name: 'Remache', 
      email: 'henry2992@hotmail.com', password: 'test1234', password_confirmation: 'test1234',
      role: 'customer')
    @property = FactoryBot.create(:property, customer: @customer)
    @company = FactoryBot.create(:company)
    @location = FactoryBot.create(:location, tax: 8.88, company_id: @company.id)
    allow(Time).to receive(:current).and_return DateTime.new(2017,8,12,22)
    @manager = FactoryBot.create(:manager)
  end

  context 'When creating new booking' do
    describe "Fee calculation" do

      it "calculates right service's costs" do
        booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property)
        base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: @company)
        service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: @company )
        extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
        extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: @company, parent: base_service )
        extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
        booking.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
        cleaner = FactoryBot.create(:cleaner, percentage_of_booking: 70)
        booking.cleaners << cleaner
        booking.save
        booking.calculate_fees
        booking.reload
        expect(booking.service_total).to eq(110.00)
        expect(booking.extras_total).to eq(240.00)
        expect(booking.sales_tax).to eq(31.08)
        expect(booking.final_amount).to eq(381.08)
        expect(CleanerBooking.first.earnings.to_f.round(2)).to eq((381.08* 70/100).round(2))
      end
    end
  end


  context 'Booking Cancelation' do

    describe 'can_cancel_booking?' do

      it 'should return true if you within the time limit to cancel a booking' do
        config = FactoryBot.create(:config, key: 'cancelation_penalty_time', value: '10')
        booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property, booking_date: DateTime.new(2017,8,12).change(hour: 8) + 1.day)
        expect(booking.can_cancel_booking?).to eq(true)
      end

      it 'should return false if you pass the time limit to cancel a booking' do
        config = FactoryBot.create(:config, key: 'cancelation_penalty_time', value: '10')
        booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property, booking_date: DateTime.new(2017,8,12).change(hour: 8) + 1.day)
        expect(booking.can_cancel_booking?).to eq(true)
      end

      it 'should return true if config is not set' do
        booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property, booking_date: DateTime.new(2017,8,12).change(hour: 8) + 1.day)
        expect(booking.can_cancel_booking?).to eq(true)
      end
    end
  end

  describe 'cancel booking' do 
    before(:each) do
      ActionMailer::Base.delivery_method = :test
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
      allow(Time).to receive(:current).and_return DateTime.new(2017,8,12,22)
      penalty_time = FactoryBot.create(:config, key: 'cancelation_penalty_time', value: '10')
      penalty_amount = FactoryBot.create(:config, key: 'cancelation_penalty_amount', value: '80')
    end

    after(:each) do
      ActionMailer::Base.deliveries.clear
    end

    context 'when penalty does not apply' do
      it 'should cancel booking if you are within the time limit' do
        booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,12).change(hour: 8) + 1.day, booking_assigned: true)
        booking.cleaners << FactoryBot.create(:cleaner)
        booking.cancel_booking
        expect(booking.booking_assigned).to eq(false)
        expect(booking.cleaners.count).to eq(0)
      end
    end

    context 'when penalty applies' do
      it 'should cancel booking and add penalties' do
        booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,12).change(hour: 8), booking_assigned: true)
        cleaner = FactoryBot.create(:cleaner)
        booking.cleaners << cleaner
        booking.cancel_booking
        expect(booking.booking_assigned).to eq(false)
        expect(booking.cleaners.count).to eq(0)
        expect(cleaner.penalties.count).to eq(1)
        expect(cleaner.penalties.first.amount).to eq(80)
        BookingMailer.notify_cancelation_to_managers(@manager, booking).deliver
        expect(ActionMailer::Base.deliveries.count).to be > (0)
      end
    end
  end

  describe 'accept customer request' do
    it 'should mark booking as assigned after accepting a request' do
      booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,20).change(hour: 8), requested: true)
      cleaner = FactoryBot.create(:cleaner)
      booking.cleaners << cleaner
      booking.accept_request
      expect(booking.cleaners).to eq([cleaner])
      expect(booking.requested).to eq(false)
      expect(booking.booking_assigned).to eq(true)
    end
  end 

  describe '#notify_cancellation' do
    it 'should notify cleaner about a cancelled booking by a customer' do
      booking =  FactoryBot.build(:booking, company: @company, location: @location, property: @property)
      base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: @company)
      service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: @company )
      extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      booking.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
      booking.cleaners << FactoryBot.create(:cleaner)
      booking.cancelled = true
      booking.save
      booking.reload
      expect(booking.cleaners.count).to eq(0)
      expect(booking.booking_assigned).to eq(false)
    end
  end

  describe 'check_expired_requested_bookings' do
    it 'should find all expired requested bookings and open them to other cleaner' do
      requested_booking_limit = FactoryBot.create(:config, key: 'requested_booking_limit', value: '24')
      booking_expired_one =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,12).change(hour: 8), requested: true)
      booking_expired_two =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,12).change(hour: 10), requested: true)
      booking_not_expired =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,15).change(hour: 20), requested: true)
      Booking.new.check_expired_requested_bookings
      expect(Booking.where(requested: false).count).to eq(2)
      expect(Booking.where(requested: true).count).to eq(1)
    end 
  end

  describe 'is_past_booking?' do
    it 'should return true if it is a past booking' do
      booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,11).change(hour: 8))
      expect(booking.is_past_booking?).to eq(true)
    end 

    it 'should return false if the booking is not in the past' do
      booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,14).change(hour: 8))
      expect(booking.is_past_booking?).to eq(false)
    end 
  end

  describe 'decline_request' do
    it "should decline a request succesfully" do
      booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,15).change(hour: 20), requested: true)
      base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: @company)
      service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: @company )
      extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      booking.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
      cleaner = FactoryBot.create(:cleaner)
      booking.cleaners << cleaner
      booking.decline_request(cleaner)
      expect(booking.cleaners.count).to eq(0)
      expect(booking.requested).to eq(false)
      expect(booking.booking_assigned).to eq(false)
    end
  end

  describe 'notify_requested_bookings' do
    it "should notify request to a cleaner succesfully" do
      booking =  FactoryBot.create(:booking, company: @company, location: @location, property: @property, 
          booking_date: DateTime.new(2017,8,15).change(hour: 20), requested: true)
      base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: @company)
      service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: @company )
      extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: @company, parent: base_service )
      extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: @company, parent: base_service )
      booking.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
      cleaner = FactoryBot.create(:cleaner)
      booking.cleaners << cleaner
      booking.save
      expect { booking.save }.to change { ActionMailer::Base.deliveries.count }.by(1)
    end
  end
end

