require 'rails_helper'

RSpec.describe Location, type: :model do
  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_presence_of(:tax) }
  end

  describe "relations" do
    it { should have_and_belong_to_many(:companies) }
    it { should have_many(:bookings) }
  end
end
