require 'rails_helper'

RSpec.describe Customer, type: :model do

  describe "validations" do
    it { is_expected.to act_as(:user) }
  end

  describe "relations" do
    it { should have_many(:properties) }
  end

  describe "Import Method" do
  	before(:each) do
      ActionMailer::Base.delivery_method = :test
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []

      @file = Rack::Test::UploadedFile
       .new(File.join(Rails.root, 'spec', 'support', 'customer', 'customers_example.csv'))
      @file_errors = Rack::Test::UploadedFile
       .new(File.join(Rails.root, 'spec', 'support', 'customer', 'customers_example_with_errors.csv'))
    end

    after(:each) do
      ActionMailer::Base.deliveries.clear
    end

    it 'should create right information with customer template' do
      expect{ Customer.import_customer(@file) }.to change{ Customer.count }.by_at_least(1)
        .and change { Property.count }.by_at_least(1)
        .and change { Tag.count }.by_at_least(1)
    end
    
    it 'should send email about import customer' do
      customer = FactoryBot.create(:customer)
      created = 20
      errors = []
      ImportCustomerMailer.send_email(customer, created, errors).deliver
      expect(ActionMailer::Base.deliveries.count).to be > (0)
    end

    it 'should create right information with customer template with errors' do
      expect{ Customer.import_customer(@file_errors) }.to change{ User.count }.from(0).to(1)
        .and change { Property.count }.from(0).to(1)
        .and change { Tag.count }.from(0).to(1)
    end
  end
end


