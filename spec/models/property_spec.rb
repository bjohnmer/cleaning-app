require 'rails_helper'

RSpec.describe Property, type: :model do
  describe "validations" do
    it { should validate_presence_of(:address) }
    it { should validate_presence_of(:city) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:postal_code) }
  end

  describe "relations" do
    it { should belong_to(:customer) }
    it { should have_many(:bookings) }
    it { should have_and_belong_to_many(:tags) }
  end
end
