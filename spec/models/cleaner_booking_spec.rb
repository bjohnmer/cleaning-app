require 'rails_helper'

RSpec.describe CleanerBooking, type: :model do
  describe "relations" do
    it { should belong_to(:cleaner) }
    it { should belong_to(:booking) }
  end
end
