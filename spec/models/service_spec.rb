require 'rails_helper'

RSpec.describe Service, type: :model do
  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:price) }
    it { should validate_presence_of(:service_type) }
    it { should define_enum_for(:service_type). with([:base, :add_on, :parameter]) }
    it { should accept_nested_attributes_for(:parameters) }
  end
  describe "relations" do
    it { should belong_to(:company) }
    it { should have_many(:parameters).class_name('Service').with_foreign_key('service_id') }
    it { should have_many(:booking_services) }
    it { should have_many(:bookings).through(:booking_services) }
  end
end

