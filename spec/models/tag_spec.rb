require 'rails_helper'

RSpec.describe Tag, type: :model do
  describe "Validations" do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end
  describe "relations" do
    it { should have_and_belong_to_many(:properties) }
  end
end
