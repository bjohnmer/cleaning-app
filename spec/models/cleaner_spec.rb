require 'rails_helper'

RSpec.describe Cleaner, type: :model do

  describe 'validations' do
  	it { is_expected.to act_as(:user) }
    it { should validate_presence_of(:address) }
    it { should validate_presence_of(:postal_code) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:city) } 
    it { should validate_numericality_of(:max_bookings).is_less_than_or_equal_to(3).is_greater_than(0) }
  end

  describe "relations" do
    it { should have_many(:cleaner_companies) }
    it { should have_many(:companies).through(:cleaner_companies)  }
    it { should have_many(:cleaner_bookings) }
    it { should have_many(:bookings).through(:cleaner_bookings)  }
    it { should have_many (:penalties) }
  end

  describe "Import Methods" do
    before(:each) do
      ActionMailer::Base.delivery_method = :test
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []

      @file = Rack::Test::UploadedFile
       .new(File.join(Rails.root, 'spec', 'support', 'cleaner', 'cleaners.csv'))
    end

    after(:each) do
      ActionMailer::Base.deliveries.clear
    end

    it 'should create right information with cleaner template' do
      expect{ 
        cleaner = FactoryBot.create(:cleaner)
        cleaner.companies.create! [
                        { name: Faker::Company.name } , 
                        { name: Faker::Company.name }
                      ]
        Cleaner.import_cleaners(@file, cleaner.companies) 
      }.to change{ Cleaner.count }.by_at_least(1)
    end
    
    it 'should send email about import cleaner' do
      cleaner = FactoryBot.create(:cleaner)
      created = 20
      errors = []
      ImportCleanerMailer.send_email(cleaner, created, errors).deliver
      expect(ActionMailer::Base.deliveries.count).to be > (0)
    end
  end

  context "Cleaner methods" do

    subject(:location) { FactoryBot.create(:location) }
    subject(:company) { FactoryBot.create(:company) }
    subject(:cleaner) { FactoryBot.create(:cleaner, company_ids: [company.id]) }
    let(:requested_booking_limit) {Config.new(key:'requested_booking_limit', value: 20)}

    before(:each) do
      allow(Time).to receive(:current).and_return DateTime.new(2018,2,22,10)
    end

    describe 'get_requested_bookings' do
      it 'should return all the requested bookings for a cleaner' do
        cleaner.bookings << FactoryBot.create(:booking, requested: true, location: location, company: company, booking_date: Time.now + 3.days)
        cleaner.bookings << FactoryBot.create(:booking, requested: true, location: location, company: company, booking_date: Time.now + 3.days)
        cleaner.bookings << FactoryBot.create(:booking, requested: true, location: location, company: company, booking_date: Time.now - 3.days)
        expect(cleaner.get_requested_bookings.count).to eq(2)
      end
    end 

    describe 'get_previous_bookings' do
      it 'should show your previos bookings correctly' do
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 3.days)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now - 10.days)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now - 3.days)
        expect(cleaner.get_previous_bookings.count).to eq(2)
      end
    end

    describe 'get_todays_bookings' do
      it "should show bookings assigned for today for specific cleaner" do 
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 3.hours)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 6.hours)
        expect(cleaner.get_todays_bookings.count).to be(2)
      end
    end

    describe 'get_tomorrow_bookings' do
      it "should show bookings assigned for tomorrow for specific cleaner" do 
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 1.day + 3.hours)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 1.day + 6.hours)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 2.days)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now - 1.day)
        expect(cleaner.get_tomorrow_bookings.count).to be(2)
      end
    end

    describe 'get_assigned_bookings_for_tomorrow' do
      it "should show bookings assigned in the future" do 
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 1.day + 3.hours)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 1.day + 6.hours)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 2.days)
        cleaner.bookings << FactoryBot.create(:booking, booking_assigned: true, location: location, company: company, booking_date: Time.now + 10.days)
        expect(cleaner.get_assigned_bookings_for_tomorrow.count).to be(2)
      end
    end
    
    describe 'tomorrow_available_bookings' do
      context 'with no search params' do
        it "should show bookings available to pick for tomorrow for cleaner" do 
          # Pick First Booking
          cleaner.bookings << FactoryBot.create(:booking, booking_date: Time.current + 1.day + 2.hours, booking_assigned: true, location: location, company: company)
          # Create one for today it should not shown
          FactoryBot.create(:booking, location: location, company: company, booking_date: Time.current)
          # it should not shown if is less than four hours ahead from the last picking booking
          FactoryBot.create(:booking, location: location, company: company, booking_date: Time.current + 1.day + 1.hour)
          # it should shown if is more than four hours ahead from the last picking booking
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 4.1.hours, location: location, company: company)
          # it should not shown if it is for the day after tomorrow 
          FactoryBot.create(:booking, booking_date: Time.current + 2.day, location: location, company: company)
          # it should not shown if booking is for yesterday
          FactoryBot.create(:booking, booking_date: Time.current - 1.day + 5.hours, location: location, company: company)
          # it should not shown if is for tomorrow but it is assigned
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, booking_assigned: true, location: location, company: company)
          # it should not shown if is for tomorrow but it is completed
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, completed: true, location: location, company: company )
          # it should not shown if is for tomorrow but it is cancelled
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, cancelled: true, location: location, company: company )
          # it should not shown if cleaner does not belong to company
          company1 = Company.create!(name: Faker::Company.name)
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, location: location, company_id: company1.id)
          # it should not shown if it is hidden
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, location: location, company: company, hidden: true)
          # it should not shown if it is requested
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 5.hours, location: location, company: company, requested: true)
          # it should show lst booking
          FactoryBot.create(:booking, location: location, company: company, booking_date: Time.current + 1.day + 6.hours)
          expect(cleaner.tomorrow_available_bookings.count).to be(1)
        end
      end

      context 'with search params' do 
        it "should show bookings available to pick for tomorrow for cleaner with params" do 
          # it should not shown if filtered with from: start_time param
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 4.1.hours, location: location, company: company)
          # it should shown if filtered with from: start_time param
          FactoryBot.create(:booking, booking_date: Time.current + 1.day + 6.hours, location: location, company: company)
          start_time = Time.current + 1.day + 5.hours
          end_time = Time.current + 1.day + 7.hours
          expect(cleaner.tomorrow_available_bookings(from: start_time, to: end_time ).count).to be(1)
        end
      end
    end

    describe 'pick_booking' do 
      it 'should pick a booking sucessfuly' do
        booking = FactoryBot.create(:booking, booking_date: Time.current, booking_assigned: false, location: location, company: company)
        cleaner.pick_booking(booking)
        expect(cleaner.bookings.count).to be(1)
        expect(booking.booking_assigned).to eq(true)
      end
    end

    describe 'can_claim_booking?' do
      it 'should return true if cleaner can clamin a booking' do 
        cleaner.bookings << FactoryBot.create(:booking, booking_date: Time.current, booking_assigned: true, location: location, company: company)
        expect(cleaner.can_claim_booking?).to be(true)
      end

      it "should return false if cleaner can't clamin a booking for today" do 
        cleaner.bookings << FactoryBot.create(:booking, booking_date: Time.current + 1.day, booking_assigned: true, location: location, company: company)
        cleaner.bookings << FactoryBot.create(:booking, booking_date: Time.current + 1.day, booking_assigned: true, location: location, company: company)
        expect(cleaner.can_claim_booking?).to be(false)
      end
    end

    describe 'has_permission_to_view?' do 
      it 'should return false if cleaner does not have permissions' do
        expect(cleaner.has_permission_to_view?).to be(false)
      end

      it 'should return false if cleaner is not allowed by permission' do
        cleaner.permission = Permission.create(name: 'Great Cleaner', hour: '10', minutes: '00')
        cleaner.save
        expect(cleaner.has_permission_to_view?).to be(false)
      end

      it 'should return true if cleaner allowed by permission' do
        cleaner.permission = Permission.create(name: 'Great Cleaner', hour: '04', minutes: '00')
        cleaner.save
        expect(cleaner.has_permission_to_view?).to be(true)
      end
    end

    describe 'bookings_left_to_claim' do 
      it 'should return number of bookings left to claim for tomorrow' do
        cleaner.bookings << FactoryBot.create(:booking, booking_date: Time.current + 1.day, booking_assigned: true, location: location, company: company)
        expect(cleaner.bookings_left_to_claim).to be(1)
      end
    end

    context 'mailing bookings' do
      before(:each) do
        ActionMailer::Base.delivery_method = :test
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries = []
      end

      describe 'email_today_bookings' do
        it 'should send emails succesfully' do
          booking1 = FactoryBot.create(:booking, booking_date: Time.current, booking_assigned: true, location: location, company: company)
          base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: company)
          service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: company )
          extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: company, parent: base_service )
          extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: company, parent: base_service )
          extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: company, parent: base_service )
          booking1.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
          cleaner.bookings << booking1
          expect { cleaner.email_today_bookings([cleaner.bookings.first.id]) }.to change { ActionMailer::Base.deliveries.count }.by(1)
        end
      end

      describe 'email_tomorrow_bookings' do
        it 'should send emails succesfully' do
          booking1 = FactoryBot.create(:booking, booking_date: Time.current, booking_assigned: true, location: location, company: company)
          base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: company)
          service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: company )
          extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: company, parent: base_service )
          extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: company, parent: base_service )
          extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: company, parent: base_service )
          booking1.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]
          cleaner.bookings << booking1
          expect { cleaner.email_tomorrow_bookings([cleaner.bookings.first.id]) }.to change { ActionMailer::Base.deliveries.count }.by(1)
        end
      end
    end
  end
end
