require 'rails_helper'

RSpec.describe Company, type: :model do
  
  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
  end

  describe "relations" do
    it { should have_and_belong_to_many(:locations) }
    it { should have_many(:services) }
    it { should have_many(:bookings) }
    it { should have_many(:cleaner_companies) }
    it { should have_many(:cleaners).through(:cleaner_companies) }
  end
end


  