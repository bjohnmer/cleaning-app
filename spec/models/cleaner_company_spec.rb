require 'rails_helper'

RSpec.describe CleanerCompany, type: :model do
  
  describe "validations" do
    it { should validate_presence_of(:cleaner) }
    it { should validate_presence_of(:company) }
  end

  describe "relations" do
    it { should belong_to(:cleaner) }
    it { should belong_to(:company) }
  end
end
