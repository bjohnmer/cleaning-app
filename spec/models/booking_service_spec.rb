require 'rails_helper'

RSpec.describe BookingService, type: :model do
  describe "relations" do
    it { should belong_to(:service) }
    it { should belong_to(:booking) }
  end
end
