require 'rails_helper'

RSpec.describe Config, type: :model do
  describe "validations" do
    it { should validate_presence_of(:key) }
    it { should validate_presence_of(:value) }
    it { should validate_uniqueness_of(:key) }
  end

  describe 'store' do
    before(:each) do
      @config = FactoryBot.create(:config)
    end

    it 'should change value correctly' do
      Config.store('cancelation_penalty_time', '24')
      @config.reload
      expect(@config.value).to eq('24')
    end
  end

  describe 'fetch' do
    before(:each) do
      @config = FactoryBot.create(:config)
    end

    it 'should return value if key was found' do
      response = Config.fetch('cancelation_penalty_time')
      expect(response).to eq('20')
    end

    it 'should return nil if key was not found' do
      response = Config.fetch('cancelation_penalty_amount')
      expect(response).to eq(nil)
    end
  end
end
