require 'rails_helper'

RSpec.describe BookingsHelper, type: :helper do

  subject(:company){FactoryBot.create(:company, name: 'Maid Sailors')}
  subject(:location){FactoryBot.create(:location, name: 'Chicago', tax: 8.88, company_id: company.id)}
  let(:service_base){FactoryBot.create(:service, name: 'Studio Apartment', price: 80.00, service_type: 'base', company: company)}
  let(:addon_service){FactoryBot.create(:service, name: 'Clean Oven', price: 10.00, service_type: 'add_on', parent: service_base, company: company)}
  let(:service_parameter){FactoryBot.create(:service, name: '1 bathroom', price: 10.00, service_type: 'parameter', service_id: service_base.id, company: company)}


  before(:each) do
    @config = FactoryBot.create(:config, key: "show_adress_time", value: "20")
    @config2 = FactoryBot.create(:config, key: "requested_booking_limit", value: "18")
    @config3 = FactoryBot.create(:config, key: "cut_of_time", value: "10")
    @customer = FactoryBot.create(:customer, first_name: 'John', last_name: 'Dow', email: 'john@email.com',
      password: 'test1234', password_confirmation: 'test1234')
    @property = FactoryBot.create(:property, customer: @customer, address: '55th st', city: 'Chicago', state: 'IL', postal_code: '60609')
    @booking = FactoryBot.create(:booking, property: @property, company: company, location: location, launch_27_id: '5', booking_date: Time.new(2017,11,2,14,0,0))
    @cleaner = FactoryBot.create(:cleaner, percentage_of_booking: 80)
    # Services
    @booking.services << service_base
    @booking.services << addon_service
    @booking.services << service_parameter
  end

  describe "#base_service" do
    it "shows base services from a list of services" do
      expect(helper.base_service(@booking.services)).to eq(service_base)
    end
  end

  describe "#params_service" do
    it "shows service paramaters from a list of services" do
      expect(helper.params_service(@booking.services)).to eq(service_parameter.name)
    end
  end

  describe "#extra_services" do
    it "shows extra services from a list of services" do
      expect(helper.extra_services(@booking.services)).to eq([addon_service])
    end
  end

 describe "#show_address" do
   context "The show_adress_time of the day before booking" do
      it "should be <= Time.now" do
        Time.stub(:now) {Time.new(2017,11,1,20,0,1)}
        expect(helper.show_address(@booking, @cleaner)).to be true
      end
      it "should not be > Time.now " do
        Time.stub(:now) {Time.new(2017,11,1,19,59,59)}
        expect(helper.show_address(@booking, @cleaner)).to be false
      end
    end
  end

  describe '#show_earnings_percetage' do
    it "should show the percentage a cleaner earns" do
      @booking.calculate_fees
      expect(helper.show_earnings_percetage(@cleaner, @booking.final_amount)).to eq(87.1)
    end
  end

  describe '#show_area' do
    it 'should show area if area exist' do 
      area = Area.create(name: 'Chicago District')
      Zipcode.create(number: 60609, area_id: area.id)
      expect(helper.show_area(@booking.property.postal_code)).to eq('Chicago District')
    end

    it 'should show area not found if area does not exist' do 
      expect(helper.show_area(@booking.property.postal_code)).to eq('No area found')
    end
  end

  describe "#show_time_left_to_claim" do
    it "returns the right time left to claim a booking for Central Time" do
      @booking = FactoryBot.create(:booking, property: @property, company: company, location: location, booking_date: Time.utc(2018,01,29,14,0,0))
      Time.stub(:now) {Time.utc(2018,01,26,20,0,0)}
      expect(helper.show_time_left_to_claim(@booking.booking_date, @cleaner)).to eq('2 days, 0 hours, 0 minutes')
    end

    it "returns the right time left to claim a booking for Pacific Time" do
      @booking = FactoryBot.create(:booking, property: @property, company: company, location: location, booking_date: Time.utc(2018,8,29,14,0,0))
      Time.stub(:now) {Time.utc(2018,8,27,11,0,0)}
      cleaner = FactoryBot.create(:cleaner, timezone: 5)
      expect(helper.show_time_left_to_claim(@booking.booking_date, cleaner)).to eq('1 days, 9 hours, 0 minutes')
    end
  end

  describe '#show_cut_of_time' do
    it 'should return string with times when a cleaner can pick a booking' do
      expect(helper.show_cut_of_time).to eq('6 PM today and 10 AM tomorrow')
    end
  end

end
