require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do

  describe "#bootstrap_class_for" do
    it "returns the right class for success messages" do
      expect(helper.bootstrap_class_for(:success)).to eq('alert-success')
    end

    it "returns the right class for error messages" do
      expect(helper.bootstrap_class_for(:error)).to eq('alert-danger')
    end

    it "returns the right class for alert messages" do
      expect(helper.bootstrap_class_for(:alert)).to eq('alert-warning')
    end

    it "returns the right class for notice messages" do
      expect(helper.bootstrap_class_for(:notice)).to eq('alert-info')
    end
  end

  describe "#flash_messages" do
    it "returns the nil on flash messages" do
      expect(helper.flash_messages(flash[:notice] = "Post successfully created")).to eq(nil)
    end
  end

  describe "#full_address" do
    it "return the right full address" do
      expect(helper.full_address('110 Bleecker Street, Apt. 5 F', 'New York', 'Ny', '10012')).to eq('110 Bleecker Street, Apt. 5 F, New York, Ny, 10012')
    end
  end
end
