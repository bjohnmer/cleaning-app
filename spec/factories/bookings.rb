FactoryBot.define do
  factory :booking do
    booking_date DateTime.now
    payment_method "Cash"
    frecuency "One Time"
    booking_assigned false
    property
    company
  end
end
