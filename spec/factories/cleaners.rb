FactoryBot.define do
  factory :cleaner do |c|
    c.email { Faker::Internet.email }
    c.password 'test1234'
    c.password_confirmation 'test1234'
    c.first_name { Faker::Name.first_name }
    c.last_name { Faker::Name.last_name }
    c.phone_number { Faker::PhoneNumber.phone_number }
    c.address { Faker::Address.street_address }
    c.postal_code { Faker::Address.zip_code }
    c.state { Faker::Address.state }
    c.city { Faker::Address.city }
    c.max_bookings 2
    c.timezone 9
    c.role 1
  end
end
