FactoryBot.define do
  factory :location do
    name Faker::Address.city
    tax 18.00
  end
end
