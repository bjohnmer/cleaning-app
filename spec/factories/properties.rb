FactoryBot.define do
  factory :property do
    # name 'Home'
    address "44 w 55St"
    city "New York"
    state "New York"
    postal_code "10009"
    customer
  end
end
