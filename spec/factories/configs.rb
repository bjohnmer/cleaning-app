FactoryBot.define do
  factory :config do
    key "cancelation_penalty_time"
    value "20"
  end
end
