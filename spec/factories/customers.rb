FactoryBot.define do
  factory :customer do
    email { Faker::Internet.email }
    password 'test1234'
    password_confirmation 'test1234'
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    role 0
  end
end
