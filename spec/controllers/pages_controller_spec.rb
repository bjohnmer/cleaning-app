require 'rails_helper'

RSpec.describe PagesController, type: :controller do

  describe "GET #index" do
    let(:cleaner) { FactoryBot.create(:cleaner) }
    before(:each) do
      sign_in cleaner
    end

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:found)
    end

    it "redirect to cleaners dashboard path" do
      get :index
      expect(response).to redirect_to(cleaners_dashboard_path)
    end
  end
end
