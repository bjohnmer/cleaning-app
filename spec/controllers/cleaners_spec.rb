require 'rails_helper'

RSpec.describe CleanersController, type: :controller do
  
  let(:cleaner) { FactoryBot.create(:cleaner) }
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
    sign_in cleaner
  end

  describe "Update Cleaner" do
    let(:valid_params) { { 'first_name' => Faker::Name.first_name,
                          'last_name' => Faker::Name.last_name } }

    let(:invalid_params) { { 'email' => nil,
                             'last_name' => nil } } 
    
    context 'when update' do

      it 'should redirect if valid params' do
        patch :update, params: { id: cleaner.id, 'cleaner' => valid_params }
        expect(response).to redirect_to edit_cleaner_path(cleaner)
      end

      it 'should flash errors if invalid params' do
        patch :update, params: { id: cleaner.id, 'cleaner' => invalid_params }
        expect( flash[:error] ).to_not be_nil
      end
    end
  end

  describe 'back_to_admin' do 
    let(:manager) { FactoryBot.create(:manager) }
    it 'should erase admin session' do
      session["admin_session_id"] = manager.user.id
      get :back_to_admin
      expect(response).to redirect_to(admin_dashboard_path)
    end
  end

  describe 'destroy_admin_session' do
    it 'should destroy admin session' do
      session["cleaner_session_id"] = cleaner.id
      get :destroy_admin_session
      expect(response).to redirect_to(root_path)
    end
  end
end
