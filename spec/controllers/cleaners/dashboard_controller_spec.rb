require 'rails_helper'

RSpec.describe Cleaners::DashboardController, type: :controller do
  
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "login" do
    let(:cleaner) { FactoryBot.create(:cleaner) }
    context "context" do
      login_cleaner

      it "should have a current_user" do
        expect(subject.current_user).to_not eq(nil)
      end

      it "should current_user be equal to cleaner" do
        expect(subject.current_user).to eq(cleaner.user)
      end

      it "should get index" do
        get 'index'
        expect(response).to be_success
      end

      it "should log out and redirect to users/sign_in" do
        sign_out cleaner
        expect(subject.current_user).to be_nil
        get 'index'
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
end