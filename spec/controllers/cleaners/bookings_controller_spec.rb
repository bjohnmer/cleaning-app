require 'rails_helper'

RSpec.describe Cleaners::BookingsController, type: :controller do
  
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  describe "login" do
    let(:cleaner) { FactoryBot.create(:cleaner) }
    context "context" do
      login_cleaner

      it "should have a current_user" do
        expect(subject.current_user).to_not eq(nil)
      end

      it "should current_user be equal to cleaner" do
        expect(subject.current_user).to eq(cleaner.user)
      end

      it "should get index" do
        get 'index'
        expect(response).to be_success
      end

      it "should log out and redirect to users/sign_in" do
        sign_out cleaner
        expect(subject.current_user).to be_nil
        get 'index'
        expect(response).to redirect_to(new_user_session_path)
      end
    end
  end
  describe "GET #calendar" do
    let(:cleaner) { FactoryBot.create(:cleaner) }
    context "context" do
      login_cleaner
      
      it "returns http success" do
        get 'calendar'
        expect(response).to be_success
      end

      it "should get json format" do
        get 'get_bookings', { :format => :json }
        expect(response).to be_success
        expect(response.content_type).to eq('application/json')
      end

      it "should get json format" do
        get 'get_booking', { :format => :json }
        expect(response).to be_success
        expect(response.content_type).to eq('application/json')
      end

    end
  end
end