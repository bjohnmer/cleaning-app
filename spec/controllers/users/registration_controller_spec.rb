require 'rails_helper'

RSpec.describe Users::RegistrationsController, type: :controller do
  
  before(:each) do
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  let(:valid_params) { {'cleaner' => {
        'email' => Faker::Internet.email,
        'password' => 'test1234',
        'password_confirmation' => 'test1234',
        'first_name' => Faker::Name.first_name,
        'last_name' => Faker::Name.last_name,
        'phone_number' => Faker::PhoneNumber.phone_number,
        'address' => Faker::Address.street_address,
        'postal_code' => Faker::Address.zip_code,
        'state' => Faker::Address.state,
        'city' => Faker::Address.city,
        'role' => :cleaner
      } } }

  let(:invalid_params) { {'cleaner' => {
        'email' => Faker::Internet.email,
        'first_name' => Faker::Name.first_name,
        'last_name' => Faker::Name.last_name
      } } }

  describe "create" do
    context "with valid attributes" do
      it 'new cleaner' do
        expect {
          post :create_cleaner, params: valid_params
        }.to change(Cleaner, :count).by(1)
      end

      it "redirects to login" do
        post :create_cleaner, params: valid_params
        expect(response).to redirect_to new_user_session_path
      end
    end

    context "with invalid attributes" do
      it 'new cleaner' do
        expect {
          post :create_cleaner, params: invalid_params
        }.to_not change(Cleaner,:count)
      end

      it "get flash error messages" do
        post :create_cleaner, params: invalid_params
        expect( flash[:error] ).to_not be_nil
      end
    end
  end
end