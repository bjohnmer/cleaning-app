require 'rails_helper'

RSpec.describe WebhooksController, type: :controller do

  
  context 'Webhooks with valid JSONS' do
    
    describe "New Booking Webhook" do
      
      before(:each) do
        @company = FactoryBot.create(:company, name: 'Maid Sailors')
        @location = FactoryBot.create(:location, name: 'Chicago', tax: 8.88, company_id: @company.id)
        @cleaner1 = FactoryBot.create(:cleaner, first_name: 'Madeleine', last_name: 'Jones', email: 'maydelene@jones.com',
          password: 'test1234', password_confirmation: 'test1234' )
      end

      it "Successfully Creates New Booking" do

        json = File.read(File.join(Rails.root, 'spec', 'support', 'webhooks', 'new_booking.json'))
        request.headers['HTTP_ZAPPIER'] = ENV['ZAPIER_WEBHOOKS_PASSWORD']
        get :new_booking, body: json
        
        # Expect Everything is created 
        expect(Booking.count).to eq(1)
        expect(Customer.count).to eq(1)
        expect(Property.count).to eq(1)
        expect(Service.count).to eq(5)
        expect(Booking.first.cleaners.count).to eq(1)

        #Expect that the objects created are correct
        expect(Customer.first.user).to have_attributes(first_name: "Luke", last_name: 'Cage', role: 'customer', 
          email: 'lukecage@email.com')
        expect(Customer.first).to have_attributes(phone: '583729202', bookings_frecuency: 'One Time Cleaning')
        expect(Property.first).to have_attributes(address: '55th St', city: 'Chicago', state: 'IL', postal_code: '60609')

        expect(Booking.first).to have_attributes(location: @location, booking_date: DateTime.new(2017,9,19, 20, 00, 00), 
          service_total: 266.76, extras_total: 115.00, sub_total: 245.00, sales_tax: 21.76, final_amount: 266.76, tip: 11.00, 
          payment_method: 'cash', frecuency: "One Time Cleaning", property: Property.first, company: @company, launch_27_id: 44 )

        expect(Service.first).to have_attributes(name: "Studio Cleaning", price: 100.00 ,service_type: "base", 
          quantity_based: false, company_id: @company.id )

        expect(Service.second).to have_attributes(name: "1 Bathroom", price: 30.00, service_type: "parameter", 
          quantity_based: false, company_id: @company.id, service_id: Service.first.id )

        # Parent for add on services
        expect(Service.find(3)).to have_attributes(name: "Balcony", price: 50.0, service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )

        expect(Service.find(4)).to have_attributes(name: "Clean Interior Windows", service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )

        expect(Service.find(5)).to have_attributes(name: "Interior Walls", service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )
      end
    end

    describe "Cancel Booking Webhook" do
      before(:each) do
        @company = FactoryBot.create(:company, name: 'Maid Sailors')
        @location = FactoryBot.create(:location, name: 'Chicago', tax: 8.88, company_id: @company.id)
        @customer = FactoryBot.create(:customer, first_name: 'John', last_name: 'Dow', email: 'john@email.com',
          password: 'test1234', password_confirmation: 'test1234')
        @property = FactoryBot.create(:property, address: '55th st', city: 'Chicago', state: 'IL', postal_code: '60609')
        @booking = FactoryBot.create(:booking, property: @property, company: @company, location: @location, launch_27_id: '5')
      end
      it "Successfully Cancels Booking" do
        json = File.read(File.join(Rails.root, 'spec', 'support', 'webhooks', 'cancel_booking.json'))
        request.headers['HTTP_ZAPPIER'] = ENV['ZAPIER_WEBHOOKS_PASSWORD']
        get :cancel_booking, body: json
        @booking.reload
        expect(@booking).to have_attributes(cancelled: true)
      end
    end

    describe "Update Booking Webhook" do
      
      before(:each) do
        @company = FactoryBot.create(:company, name: 'Maid Sailors')
        @location = FactoryBot.create(:location, name: 'Chicago', tax: 8.88, company_id: @company.id)
        @cleaner1 = FactoryBot.create(:cleaner, first_name: 'Madeleine', last_name: 'Jones', email: 'maydelene@jones.com',
          password: 'test1234', password_confirmation: 'test1234' )
        @cleaner2 = FactoryBot.create(:cleaner, first_name: 'Moses', last_name: 'Leonard', email: 'leonard@email.com',
          password: 'test1234', password_confirmation: 'test1234' )
        @customer = FactoryBot.create(:customer, first_name: 'Luke', last_name: 'Cage', email: 'lukecage@email.com',
          password: 'test1234', password_confirmation: 'test1234')
        @property = FactoryBot.create(:property, address: '55th st', city: 'Chicago', state: 'IL', postal_code: '60609')
        @booking = FactoryBot.create(:booking, property: @property, company: @company, location: @location, launch_27_id: '5')
      end

      it "Successfully Updates New Booking" do

        json = File.read(File.join(Rails.root, 'spec', 'support', 'webhooks', 'update_booking.json'))
        request.headers['HTTP_ZAPPIER'] = ENV['ZAPIER_WEBHOOKS_PASSWORD']
        get :update_booking, body: json
        
        # Expect Everything is created 
        expect(Booking.count).to eq(1)
        expect(Customer.count).to eq(1)
        expect(Property.count).to eq(2)
        expect(Service.count).to eq(5)
        expect(Booking.first.cleaners.count).to eq(2)

        #Expect that the objects created are correct
        expect(Booking.first).to have_attributes(location: @location, booking_date: DateTime.new(2017,9,19, 20, 00, 00), 
          service_total: 266.76, extras_total: 115.00, sub_total: 245.00, sales_tax: 21.76, final_amount: 266.76, tip: 11.00, 
          payment_method: 'cash', frecuency: "One Time Cleaning", property: Property.first, company: @company, launch_27_id: 5 )

        expect(@booking.services.first).to have_attributes(name: "Studio Cleaning", price: 100.00 ,service_type: "base", 
          quantity_based: false, company_id: @company.id )

        expect(@booking.services.second).to have_attributes(name: "1 Bathroom", price: 30.00, service_type: "parameter", 
          quantity_based: false, company_id: @company.id, service_id: Service.first.id )

        # # Parent for add on services
        expect(@booking.services.third).to have_attributes(name: "Balcony", price: 50.0, service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )

        expect(@booking.services.fourth).to have_attributes(name: "Clean Interior Windows", service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )

        expect(@booking.services.fifth).to have_attributes(name: "Interior Walls", service_type: "add_on", 
          quantity_based: false, company_id: @company.id, parent_id: Service.first.id )
      end
    end

    describe "Complete Booking Webhook" do
      before(:each) do
        @company = FactoryBot.create(:company, name: 'Maid Sailors')
        @location = FactoryBot.create(:location, name: 'Chicago', tax: 8.88, company_id: @company.id)
        @customer = FactoryBot.create(:customer, first_name: 'John', last_name: 'Dow', email: 'john@email.com',
          password: 'test1234', password_confirmation: 'test1234')
        @property = FactoryBot.create(:property, address: '55th st', city: 'Chicago', state: 'IL', postal_code: '60609')
        @booking = FactoryBot.create(:booking, property: @property, company: @company, location: @location, launch_27_id: '5')
      end
      it "Successfully Complete Booking" do
        json = File.read(File.join(Rails.root, 'spec', 'support', 'webhooks', 'complete_booking.json'))
        request.headers['HTTP_ZAPPIER'] = ENV['ZAPIER_WEBHOOKS_PASSWORD']
        get :complete_booking, body: json
        @booking.reload
        expect(@booking).to have_attributes(completed: true)
      end
    end


  end
end
