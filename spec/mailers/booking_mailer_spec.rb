require "rails_helper"

RSpec.describe BookingMailer, type: :mailer do

  subject (:user) { FactoryBot.create(:cleaner) }
  subject (:manager ) { FactoryBot.create(:manager) }
  subject(:location) { FactoryBot.create(:location) }
  let(:booking) { FactoryBot.create(:booking, location: location) }
  subject (:cleaner) { FactoryBot.create(:cleaner, send_emails: true) }

  before (:each) do 
    base_service = FactoryBot.create(:service, name: "Studio Apartment", price: 80.00, service_type: 'base', company: booking.company)
    service_parameter = FactoryBot.create(:service, name: "One Bathroom", price: 30.00, service_type: 'parameter', company: booking.company )
    extra_service_1 = FactoryBot.create(:service, name: "Deep Cleaning", price: 100.00, service_type: 'add_on', company: booking.company, parent: base_service )
    extra_service_2 = FactoryBot.create(:service, name: "Inside the Cabinets", price: 40.00, service_type: 'add_on', company: booking.company, parent: base_service )
    extra_service_3 = FactoryBot.create(:service, name: "Interior Walss", price: 100.00, service_type: 'add_on', company: booking.company, parent: base_service )
    booking.services << [base_service, service_parameter, extra_service_1, extra_service_2, extra_service_3]   
  end

  describe 'notify_cancelation' do 

    let(:mail) { described_class.notify_cancelation(user, booking).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Your Planned Booking was cancelled')
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @booking customer' do
      expect(mail.body.encoded)
          .to match(booking.property.customer.full_name)
    end

    context 'when env is local' do
      it 'renders the receiver email' do
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end

    context 'when env is servuspros' do
      it 'renders the receiver email' do
        ENV['ROOT_URL'] = 'http://servuspros.com'
        expect(mail.to).to eq([user.email])
      end
    end
  end

  describe 'notify_cancelation_to_managers' do 

    let(:mail) { described_class.notify_cancelation_to_managers(user, booking).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Important! '+ booking.property.customer.full_name+' cancelled booking')
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @user' do
      expect(mail.body.encoded).to match(user.full_name)
    end

    it 'assigns @booking customer' do
      expect(mail.body.encoded)
        .to match(booking.property.customer.full_name)
    end

    context 'when env is local' do
      it 'renders the receiver email' do
        ENV['ROOT_URL'] = nil
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end

    context 'when env is servuspros' do
      it 'renders the receiver email' do
        ENV['ROOT_URL'] = 'http://servuspros.com'
        expect(mail.to).to eq([user.email])
      end
    end
  end

  describe 'notify_declined_booking_to_managers' do 

    let(:mail) { described_class.notify_declined_booking_to_managers(manager, booking, user).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Important! ' + user.full_name+ 'has declined a requested booking.')
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @cleaner' do
      expect(mail.body.encoded).to match(user.full_name)
    end

    it 'assigns @manager' do
      expect(mail.body.encoded).to match(manager.full_name)
    end

    it 'assigns @booking customer' do
      expect(mail.body.encoded)
        .to match(booking.property.customer.full_name)
    end

    context 'when env is local' do
      it 'renders the receiver email' do
        ENV['ROOT_URL'] = nil
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end

    context 'when env is servuspros' do
      it 'renders the receiver email' do
        ENV['ROOT_URL'] = 'http://servuspros.com'
        expect(mail.to).to eq([manager.email])
      end
    end
  end

  describe 'email_today_bookings' do 

    before (:each) do 
      @bookings = []
      @bookings << booking
    end

    let(:mail) { described_class.email_today_bookings(user, @bookings).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Here’s ' + user.full_name+ 'schedule for ' + Time.current.strftime("%m/%d/%Y"))
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @cleaner' do
      expect(mail.body.encoded).to match(user.full_name)
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    context 'when cleaner does not receive emails' do
      it 'renders the receiver email' do
        user.send_emails = false
        user.save
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end
  end

  describe 'email_tomorrow_bookings' do 

    before (:each) do 
      @bookings = []
      @bookings << booking
    end
    let(:mail) { described_class.email_tomorrow_bookings(user, @bookings).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('Here’s ' + user.full_name+ 'schedule for ' + (Time.current + 1.day).strftime("%m/%d/%Y"))
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @cleaner' do
      expect(mail.body.encoded).to match(user.full_name)
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    context 'when cleaner does not receive emails' do
      it 'renders the receiver email' do
        user.send_emails = false
        user.save
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end
  end

  describe 'email_customer_request' do 
    let(:mail) { described_class.email_customer_request(cleaner, booking).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq('You have a new requested booking')
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'assigns @cleaner' do
      expect(mail.body.encoded).to match(cleaner.full_name)
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([cleaner.email])
    end

    context 'when cleaner receives emails' do
      it 'renders the receiver email' do
        expect(mail.to).to eq([cleaner.email])
      end
    end

    context 'when cleaner does not receive emails' do
      it 'renders the receiver email' do
        cleaner.send_emails = false
        cleaner.save
        expect(mail.to).to eq(["theservusapp@gmail.com"])
      end
    end
  end
end