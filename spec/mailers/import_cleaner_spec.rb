require "rails_helper"

RSpec.describe ImportCleanerMailer, type: :mailer do

  describe 'send_email' do
    let(:user) { FactoryBot.create(:cleaner) }
    let(:mail) { 
      created = 20
      errors = []
      described_class.send_email(user, created, errors).deliver_now 
    }

    it 'renders the subject' do
      expect(mail.subject).to eq('Import Cleaner Email')
    end

    it 'renders the sender\'s email' do
      expect(mail.from).to eq(['info@servuspros.com'])
    end

    it 'renders the receiver\'s email' do
      expect(mail.to).to eq([user.email])
    end

  end
end