# Preview all emails at http://localhost:3000/rails/mailers/example_mailer
class ImportCustomerPreview < ActionMailer::Preview
  def sample_mail_preview
    created = 20
    errors = []
    ImportCustomerMailer.send_email(User.first, created, errors)
  end
end