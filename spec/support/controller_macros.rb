module ControllerMacros
  def login_cleaner
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:cleaner]
      sign_in cleaner
    end
  end
end