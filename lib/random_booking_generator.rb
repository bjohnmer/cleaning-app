class RandomBookingGenerator
  def self.generate_bookings(number, start_date, end_date)

    location = Location.order("RANDOM()").first
    company = Company.order("RANDOM()").first
    (0...number).each do |number|
      cst = {
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.email
      }
      ppt = {
        street_address: Faker::Address.street_address,
        secondary_address: Faker::Address.secondary_address,
        city: Faker::Address.city,
        state: Faker::Address.state,
        zip_code: Faker::Address.zip_code
      }
      customer = Customer.create!(first_name: cst[:first_name], last_name: cst[:last_name], email: cst[:email], 
          password: 'test1234', password_confirmation: 'test1234', role: 'customer')
      property = Property.create!(address: "#{ppt[:street_address]}, #{ppt[:secondary_address]}", city: ppt[:city], state: ppt[:state], postal_code: ppt[:zip_code], customer: customer)    
      
      # min = rand(0..1) == 1 ? 30 : 0
      first_date = Date.parse(start_date)
      secon_date = Date.parse(end_date)
      selected_date = rand(first_date..secon_date)

      start_d = Time.parse(selected_date.strftime("%Y-%m-%d 08:00:00 UTC"))
      end_d = Time.parse(selected_date.strftime("%Y-%m-%d 18:00:00 UTC"))
      
      time = rand(start_d..end_d).localtime
      time = Time.new(time.year, time.month, time.day, time.hour, 0, 0)

      booking =  Booking.create!(booking_date: time, company: company, location: location, property: property, booking_assigned: false, cancelled: false)
      base_service = Service.where(service_type: 'base').order("RANDOM()").first
      if base_service.parameters.empty?
        service_parameter = base_service.parameters.create!(name: 'Example Service Parameter', price: 10.0, service_type: 'parameter', company: company)
      else
        service_parameter = base_service.parameters.order("RANDOM()").first
      end
      extra_services = base_service.children.order("RANDOM()").limit(3)
      booking.services << [base_service, service_parameter, extra_services]
      booking.calculate_fees
      booking.save
    end
  end
end
