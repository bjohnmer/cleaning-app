class CopyTempFile
  def self.copy_file(file, importer_dir_path)
    original_filename = file.original_filename
    extname = File.extname(original_filename)
    basename = File.basename(original_filename, extname)
    temp_file = Tempfile.new([basename, extname], importer_dir_path)
    #create new file
    new_file = temp_file.to_path.to_s
    temp_file.close
    temp_file.unlink
    FileUtils.cp(file.tempfile.to_path.to_s, new_file)  
    return (new_file)
  end
end