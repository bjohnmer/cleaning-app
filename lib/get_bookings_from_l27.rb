class GetBookingsFromL27

  def self.save_bookings(start_date, end_date)
    l27_bookings = GetBookingsFromL27.get_bookings(start_date, end_date)
    l27_bookings.each do |b|
      booking_servus = Booking.find_by(launch_27_id: b['id'])
      unless booking_servus
        company = Company.find_by(name: 'Maid Sailors')
        customer = create_customer(b['user']['first_name'], b['user']['last_name'], b['phone'], b['user']['email'])
        property = create_properties(b['address']['street'], b['address']['city'], b['address']['state'], b['address']['zip'], customer)
        add_property_to_customer(customer, property)
        if customer.save!
          location = create_location(b['summary']['tax']['tax_percent'], b['location']['name'], b['address']['state'])
          if location
            booking = create_booking(location, b, property, company) 
            # Get Services
            base_service = get_base_service(b['services'].first['name'], b['services'].first['price'], company)
            booking.services << base_service
            if b['services'].first['pricing_parameters']
              pricing_parameter = get_parameter_service(b['services'].first['pricing_parameters'].first['name'], b['services'].first['pricing_parameters'].first['price'], base_service, company, booking)
            end
            get_extra_services(b['services'].first['extras'], booking, base_service, company)
            #Get Team
            assign_teams(b['assigned_teams'], booking)
            if booking.save!
              Rails.logger.info("Booking Created Sucessfully from launch27 API")
            else
              notify_failure_message("Error on creating Booking from launch27 API")
            end    
          else
            Rails.logger.info("Error on creating Booking from launch27 API")
          end
        else
          notify_failure_message("Error on creating Booking from launch27 API")
          Rails.logger.info("Error on creating Customer from launch27 API")
        end
      end
    end
  end

  def self.get_bookings(start_date = Date.today, end_date = Date.today)
    start_date = start_date.strftime("%Y-%m-%d") 
    end_date = end_date.strftime("%Y-%m-%d") 
    sat = get_single_token
    connection = Faraday.new( url: 'https://maidsailors.launch27.com')
    auth = ENV['LAUNCH_27_EMAIL'] + ':' + sat
    response = connection.get do |req|
      req.url "/v1/staff/bookings?from=#{start_date}&to=#{end_date}"
      req.headers['Authorization'] = [auth]
    end
    response = JSON.parse(response.body)
  end

  def self.get_single_token
    connection = Faraday.new( url: 'https://maidsailors.launch27.com')
    body = {
      email: ENV['LAUNCH_27_EMAIL'],
      password:ENV['LAUNCH_27_PASSWORD']
    }
    response = connection.post do |req|
      req.headers['Content-Type'] = 'application/json'
      req.url '/v1/login'
      req.body = body.to_json
    end
    response = JSON.parse(response.body)
    response = response['single_access_token']
  end


  private

  def self.create_customer(f_name, l_name, phone, email)
    name = set_name(f_name, l_name)
    first_name = name[0]
    last_name = name[1]
    customer = Customer.create_with(
      first_name: first_name,
      last_name: last_name,
      phone: phone,
      role: 'customer',
    ).find_or_create_by(email: email)
    customer.skip_password_validation = true
    customer
  end

  def self.set_name(f_name, l_name)
    first_name = f_name
    last_name = l_name
    if l_name == ""
      first_name = f_name.split(' ').first
      last_name = f_name.split(' ').second
    end
    last_name = 'No last name' if last_name == nil
    return first_name, last_name
  end

  def self.create_properties(address, city, state, zip, customer)
    property = Property.create_with(
      state: state,
      postal_code: zip, 
      city: city
    ).find_or_create_by(address: address, customer: customer)
  end

  def self.add_property_to_customer(customer, property)
    unless customer.properties.pluck(:address).include?(property.address)
      customer.properties << property
    end
  end

  def self.create_location(tax, city, state)
    location = Location.create_with(tax: tax.to_f).find_or_create_by(name: city)
    # if location does not exists find by state 
    unless location
      if state == 'NY'
        location = Location.find_by(name: 'New York')
      elsif state == 'MA'
        location = Location.find_by(name: 'Boston')
      elsif state == 'NJ'
        location = Location.find_by(name: 'New Jersey')
      elsif state == 'IL'
        location = Location.find_by(name: 'Chicago')
      else
        location = Location.find_by(name: 'New York')
      end
    end
    location
  end

  def self.create_booking(location, b, property, company)
    discount = b['summary']['discount'].to_f
    time = Time.parse(b['service_date']).in_time_zone('Eastern Time (US & Canada)')
    time = Time.zone.local_to_utc(time)
    booking = Booking.create_with(
      location: location,
      booking_date: time,
      service_total: b['summary']['total'].to_f,
      extras_total: b['summary']['extras'].to_f,
      sub_total: b['summary']['revenue'].to_f,
      sales_tax: b['summary']['tax']['tax_amount'].to_f,
      final_amount: b['summary']['total'].to_f,
      tip: b['summary']['tip'].to_f,
      payment_method: b['payment_method_info']['code'],
      frecuency: b['frequency']['name'],
      giftcard_amount_used: 0,
      customer_notes: b['customer_notes'],
      discount_from_frecuency: discount,
      property: property,
      company: company,
    ).find_or_create_by(launch_27_id: b['id'])
    #Reinstate a booking if it was cancelled
    if booking.cancelled == true
      booking.cancelled = false
    end
    booking
  end

  def self.get_base_service(service, price, company)
    base_service = company.services.create_with(service_type: 'base').find_or_create_by(name: service, price: price.to_f)
    base_service
  end

  def self.get_parameter_service(parameter, price, base_service, company, booking)
    if parameter
      pricing_parameter = company.services.create_with(service_type: 'parameter').find_or_create_by(name: parameter, price: price)
      base_service.parameters << pricing_parameter
      booking.services << pricing_parameter
    end
  end
  
  def self.get_extra_services(extras, booking, base_service, company)
    if extras 
      extras.each do |extra|
        booking.services << company.services.create_with(service_type: 'add_on').find_or_create_by(name: extra['name'], price: extra['price'], ancestry: base_service.id )
      end
    end
  end

  def self.assign_teams(team, booking)
    if team
      team.each do |t|
        cleaner = Cleaner.find_by(full_name: t['title'])
        booking.cleaners << cleaner if cleaner
      end
      booking.requested = true
    end
  end

  def self.notify_failure_message(message)
    notifier = Slack::Notifier.new "https://hooks.slack.com/services/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" do
      defaults channel: "#webhooks_notifier",
              username: "Webhook notifier"
    end
    notifier.ping(message)
  end
end
