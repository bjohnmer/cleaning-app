namespace :bookings do
  desc "Rake task to find requested bookings"
  task requested_bookings: :environment do
  	puts "Updating Requested Bookings"
  	Booking.new.check_expired_requested_bookings
    puts "#{Time.now} — Success!"
  end
end
