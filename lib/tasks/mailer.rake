namespace :mailer do
  desc "Send Tomorrow Bookings for all cleaners"
  task email_tomorrow_bookings: :environment do
    puts "Sending Emails to Cleaners"
    cleaners = Cleaner.where('archived != ?', true)
    cleaners.each do |cleaner|
      bookings = cleaner.get_bookings_by_date(Time.now.localtime.beginning_of_day + 1.days, Time.now.localtime.beginning_of_day + 1.days)
      unless bookings.empty?
        cleaner.email_tomorrow_bookings(bookings)
      end
    end 
  end
end
